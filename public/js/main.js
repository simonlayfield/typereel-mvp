
let fileSelectInput = document.getElementById('specimenFile');

if (fileSelectInput) {
  fileSelectInput.addEventListener('change', function() {
    document.getElementById('specimenLabel').innerHTML = 'File selected &check;';
  });
}
