(function () {
    'use strict';

    function noop() { }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function svg_element(name) {
        return document.createElementNS('http://www.w3.org/2000/svg', name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_data(text, data) {
        data = '' + data;
        if (text.data !== data)
            text.data = data;
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }
    class HtmlTag {
        constructor(anchor = null) {
            this.a = anchor;
            this.e = this.n = null;
        }
        m(html, target, anchor = null) {
            if (!this.e) {
                this.e = element(target.nodeName);
                this.t = target;
                this.h(html);
            }
            this.i(anchor);
        }
        h(html) {
            this.e.innerHTML = html;
            this.n = Array.from(this.e.childNodes);
        }
        i(anchor) {
            for (let i = 0; i < this.n.length; i += 1) {
                insert(this.t, this.n[i], anchor);
            }
        }
        p(html) {
            this.d();
            this.h(html);
            this.i(this.a);
        }
        d() {
            this.n.forEach(detach);
        }
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if ($$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    /* src/components/client/Specimen.svelte generated by Svelte v3.23.0 */

    function create_else_block(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");

    			div.innerHTML = `<svg class="icon -warning" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"></path><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-1-7v2h2v-2h-2zm2-1.645A3.502 3.502 0 0 0 12 6.5a3.501 3.501 0 0 0-3.433 2.813l1.962.393A1.5 1.5 0 1 1 12 11.5a1 1 0 0 0-1 1V14h2v-.645z"></path></svg>
       Unidentified
    `;

    			attr(div, "class", "meta-bar");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (8:49) 
    function create_if_block_1(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");

    			div.innerHTML = `<svg class="icon -notify" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path fill="none" d="M0 0h24v24H0z"></path><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zM6.243 11.686l.802-2.473c1.846.65 3.187 1.214 4.023 1.69-.22-2.102-.337-3.548-.348-4.337h2.525c-.034 1.149-.168 2.589-.4 4.319 1.196-.604 2.566-1.161 4.11-1.672l.802 2.473a21.708 21.708 0 0 1-4.337.976c.708.615 1.707 1.713 2.995 3.292l-2.09 1.48c-.673-.917-1.469-2.165-2.386-3.744-.859 1.637-1.614 2.885-2.264 3.744l-2.056-1.48c1.347-1.661 2.311-2.758 2.892-3.292a62.69 62.69 0 0 1-4.268-.976z"></path></svg>
       Similar
    `;

    			attr(div, "class", "meta-bar");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (3:2) {#if specimenData.status == 'identified'}
    function create_if_block(ctx) {
    	let div;
    	let svg;
    	let path0;
    	let path1;
    	let t0;
    	let t1_value = /*specimenData*/ ctx[0].name + "";
    	let t1;

    	return {
    		c() {
    			div = element("div");
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			t0 = space();
    			t1 = text(t1_value);
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path1, "d", "M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-.997-6l7.07-7.071-1.414-1.414-5.656 5.657-2.829-2.829-1.414 1.414L11.003 16z");
    			attr(svg, "class", "icon -highlight");
    			attr(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr(svg, "viewBox", "0 0 24 24");
    			attr(svg, "width", "24");
    			attr(svg, "height", "24");
    			attr(div, "class", "meta-bar");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, svg);
    			append(svg, path0);
    			append(svg, path1);
    			append(div, t0);
    			append(div, t1);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*specimenData*/ 1 && t1_value !== (t1_value = /*specimenData*/ ctx[0].name + "")) set_data(t1, t1_value);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    function create_fragment(ctx) {
    	let div;

    	function select_block_type(ctx, dirty) {
    		if (/*specimenData*/ ctx[0].status == "identified") return create_if_block;
    		if (/*specimenData*/ ctx[0].status == "suggestions") return create_if_block_1;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	return {
    		c() {
    			div = element("div");
    			if_block.c();
    			attr(div, "class", "ui-bar");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			if_block.m(div, null);
    		},
    		p(ctx, [dirty]) {
    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div, null);
    				}
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(div);
    			if_block.d();
    		}
    	};
    }

    function instance($$self, $$props, $$invalidate) {
    	let { specimenData = globalData } = $$props;

    	$$self.$set = $$props => {
    		if ("specimenData" in $$props) $$invalidate(0, specimenData = $$props.specimenData);
    	};

    	return [specimenData];
    }

    class Specimen extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance, create_fragment, safe_not_equal, { specimenData: 0 });
    	}
    }

    /* src/components/client/Reel.svelte generated by Svelte v3.23.0 */

    const { document: document_1 } = globals;

    function add_css() {
    	var style = element("style");
    	style.id = "svelte-1taq9ra-style";
    	style.textContent = ".reel.svelte-1taq9ra{display:grid;max-width:1440px;margin-left:auto;margin-right:auto}@media(min-width: 500px){.reel.svelte-1taq9ra{grid-template-columns:repeat(3, 1fr)}}@media(min-width: 760px){.reel.svelte-1taq9ra{grid-template-columns:repeat(4, 1fr)}}@media(min-width: 1200px){.reel.svelte-1taq9ra{grid-template-columns:repeat(5, 1fr)}}";
    	append(document_1.head, style);
    }

    function create_fragment$1(ctx) {
    	let section;

    	return {
    		c() {
    			section = element("section");

    			section.innerHTML = `<div class="edge"></div> 
  <div class="reel svelte-1taq9ra" id="reel"></div>`;
    		},
    		m(target, anchor) {
    			insert(target, section, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(section);
    		}
    	};
    }

    function getShortestColumn() {
    	const reel_columns = document.querySelectorAll(".column_inner");
    	let lowestIndex = 0;

    	for (let i = 1; i < reel_columns.length; i++) {
    		if (reel_columns[i].clientHeight < reel_columns[lowestIndex].clientHeight) lowestIndex = i;
    	}

    	return lowestIndex ? lowestIndex : 0;
    }

    function fadeInImage(specimenEl) {
    	setTimeout(
    		function () {
    			specimenEl.classList.add("-thisthing");
    		},
    		400
    	);
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let { reelData } = $$props, { page } = $$props;
    	let numColumns = 1;
    	let reel;
    	var screenIsXLarge = window.matchMedia("(min-width: 1200px)");
    	var screenIsLarge = window.matchMedia("(min-width: 760px) and (max-width: 1199px)");
    	var screenIsMedium = window.matchMedia("(min-width: 500px) and (max-width: 759px)");
    	widthChange();
    	screenIsXLarge.addListener(widthChange);
    	screenIsLarge.addListener(widthChange);
    	screenIsMedium.addListener(widthChange);

    	function widthChange() {
    		if (screenIsXLarge.matches) {
    			numColumns = 5;
    		} else if (screenIsLarge.matches) {
    			numColumns = 4;
    		} else if (screenIsMedium.matches) {
    			numColumns = 3;
    		} else {
    			numColumns = 1;
    		}

    		reelInit(reel);
    	}

    	onMount(async () => {
    		reel = document.getElementById("reel");
    		reelInit(reel);
    	});

    	async function reelInit(reel) {
    		if (!reel) {
    			return;
    		}

    		reel.innerHTML = "";

    		for (let i = 0; i < numColumns; i++) {
    			let column = document.createElement("div");
    			let columnInner = document.createElement("div");
    			column.className = "column";
    			columnInner.className = "column_inner";
    			columnInner.style.minHeight = `${i * 5}px`;
    			column.appendChild(columnInner);
    			reel.appendChild(column);
    		}

    		const targetPage = page == "reel" ? "specimen" : "feature";
    		const reel_columns = document.querySelectorAll(".column_inner");

    		for (let i = 0; i < reelData.length; i++) {
    			let specimenEl = document.createElement("div"),
    				specimenLink = document.createElement("a"),
    				specimenImg = new Image();

    			specimenEl.className = "specimen";
    			specimenLink.className = "image-link";
    			specimenLink.href = `/${targetPage}/${reelData[i]._id}`;
    			specimenImg.className = "specimen-image";
    			specimenImg.id = `specimen${i}`;

    			// debugger;
    			// Set specimen status
    			if (reelData[i].name) {
    				$$invalidate(0, reelData[i].status = "identified", reelData);
    			} else {
    				if (reelData[i].comments && reelData[i].comments.length) {
    					for (var comment of reelData[i].comments) {
    						if (comment.type == "similar" || comment.type == "match") {
    							$$invalidate(0, reelData[i].status = "suggestions", reelData);
    							break;
    						}
    					}
    				}
    			}

    			const load = url => new Promise(resolve => {
    					specimenImg.onload = () => resolve({
    						url,
    						ratio: specimenImg.naturalWidth / specimenImg.naturalHeight
    					});

    					specimenImg.src = url;
    				});

    			const { url, ratio } = await load(`https://res.cloudinary.com/typereel/image/upload/f_auto,q_70,w_828/${reelData[i].image.src}`);

    			if (targetPage == "specimen") {
    				var specimen = new Specimen({
    						target: specimenEl,
    						props: { specimenData: reelData[i] }
    					});
    			}

    			specimenLink.appendChild(specimenImg);
    			specimenEl.insertBefore(specimenLink, specimenEl.childNodes[0]);
    			reel_columns[getShortestColumn()].appendChild(specimenEl);

    			// TODO Review this - there's probably a better
    			// way of doing this
    			fadeInImage(specimenEl);
    		}
    	}

    	$$self.$set = $$props => {
    		if ("reelData" in $$props) $$invalidate(0, reelData = $$props.reelData);
    		if ("page" in $$props) $$invalidate(1, page = $$props.page);
    	};

    	return [reelData, page];
    }

    class Reel extends SvelteComponent {
    	constructor(options) {
    		super();
    		if (!document_1.getElementById("svelte-1taq9ra-style")) add_css();
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, { reelData: 0, page: 1 });
    	}
    }

    /* src/components/client/CommentForm.svelte generated by Svelte v3.23.0 */

    function create_fragment$2(ctx) {
    	let div2;
    	let form;
    	let div0;
    	let t2;
    	let div1;
    	let form_action_value;

    	return {
    		c() {
    			div2 = element("div");
    			form = element("form");
    			div0 = element("div");

    			div0.innerHTML = `<label for="comment" class="label">Comment</label> 
      <textarea name="comment" id="comment" cols="30" rows="10" class="textarea _padded-min" value="
      "></textarea>`;

    			t2 = space();
    			div1 = element("div");
    			div1.innerHTML = `<input type="submit" value="Submit Comment" class="ui-button">`;
    			attr(div0, "class", "form-group");
    			attr(div1, "class", "form-group _spaced-med");
    			attr(form, "action", form_action_value = "/comments/addComment/" + /*specimenType*/ ctx[0] + "/" + /*specimenId*/ ctx[1]);
    			attr(form, "method", "post");
    			attr(form, "enctype", "application/x-www-form-urlencoded");
    			attr(div2, "class", "_spaced-tb-med");
    		},
    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, form);
    			append(form, div0);
    			append(form, t2);
    			append(form, div1);
    		},
    		p(ctx, [dirty]) {
    			if (dirty & /*specimenType, specimenId*/ 3 && form_action_value !== (form_action_value = "/comments/addComment/" + /*specimenType*/ ctx[0] + "/" + /*specimenId*/ ctx[1])) {
    				attr(form, "action", form_action_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(div2);
    		}
    	};
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let { specimenType } = $$props;
    	let { specimenId } = $$props;

    	$$self.$set = $$props => {
    		if ("specimenType" in $$props) $$invalidate(0, specimenType = $$props.specimenType);
    		if ("specimenId" in $$props) $$invalidate(1, specimenId = $$props.specimenId);
    	};

    	return [specimenType, specimenId];
    }

    class CommentForm extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, { specimenType: 0, specimenId: 1 });
    	}
    }

    /* src/components/client/SuggestionForm.svelte generated by Svelte v3.23.0 */

    function create_fragment$3(ctx) {
    	let div11;
    	let form;
    	let div1;
    	let t2;
    	let div2;
    	let t4;
    	let div3;
    	let t7;
    	let div4;
    	let t10;
    	let div5;
    	let t12;
    	let div7;
    	let t15;
    	let div9;
    	let t18;
    	let div10;
    	let form_action_value;

    	return {
    		c() {
    			div11 = element("div");
    			form = element("form");
    			div1 = element("div");

    			div1.innerHTML = `<div><input type="checkbox" name="type" id="hand" value="hand" on-change="toggleOptions"> 
        <label for="hand" class="label">
          This specimen contains hand lettering
        </label></div>`;

    			t2 = space();
    			div2 = element("div");
    			div2.textContent = "- OR -";
    			t4 = space();
    			div3 = element("div");

    			div3.innerHTML = `<label for="comment" class="label">Font name</label> 
      <input type="text" name="comment" id="comment">`;

    			t7 = space();
    			div4 = element("div");

    			div4.innerHTML = `<label for="url" class="label">Reference URL</label> 
      <input type="text" name="url" id="url" placeholder="e.g. http://www.myfonts.com/...">`;

    			t10 = space();
    			div5 = element("div");
    			div5.textContent = "The likeness to this font is...";
    			t12 = space();
    			div7 = element("div");

    			div7.innerHTML = `<div><input type="radio" name="type" id="likeness-similar" value="similar" on-change="toggleOptions"> 
        <label for="likeness-similar" class="label">Similar</label></div>`;

    			t15 = space();
    			div9 = element("div");

    			div9.innerHTML = `<div><input type="radio" name="type" id="likeness-exact" value="match" on-change="toggleOptions"> 
        <label for="likeness-exact" class="label">Exact</label></div>`;

    			t18 = space();
    			div10 = element("div");
    			div10.innerHTML = `<input type="submit" value="Submit Suggestion" class="ui-button">`;
    			attr(div1, "class", "form-group _spaced-b-min");
    			attr(div2, "class", "_spaced-med");
    			attr(div3, "class", "form-group _spaced-b-min");
    			attr(div4, "class", "form-group _spaced-min");
    			attr(div5, "class", "_spaced-med");
    			attr(div7, "class", "form-group _spaced-b-min");
    			attr(div9, "class", "form-group _spaced-b-min");
    			attr(div10, "class", "form-group _spaced-med");
    			attr(form, "action", form_action_value = "/comments/addComment/" + /*specimenType*/ ctx[0] + "/" + /*specimenId*/ ctx[1]);
    			attr(form, "method", "post");
    			attr(form, "enctype", "application/x-www-form-urlencoded");
    			attr(div11, "class", "_padded");
    		},
    		m(target, anchor) {
    			insert(target, div11, anchor);
    			append(div11, form);
    			append(form, div1);
    			append(form, t2);
    			append(form, div2);
    			append(form, t4);
    			append(form, div3);
    			append(form, t7);
    			append(form, div4);
    			append(form, t10);
    			append(form, div5);
    			append(form, t12);
    			append(form, div7);
    			append(form, t15);
    			append(form, div9);
    			append(form, t18);
    			append(form, div10);
    		},
    		p(ctx, [dirty]) {
    			if (dirty & /*specimenType, specimenId*/ 3 && form_action_value !== (form_action_value = "/comments/addComment/" + /*specimenType*/ ctx[0] + "/" + /*specimenId*/ ctx[1])) {
    				attr(form, "action", form_action_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(div11);
    		}
    	};
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let { specimenType } = $$props;
    	let { specimenId } = $$props;

    	$$self.$set = $$props => {
    		if ("specimenType" in $$props) $$invalidate(0, specimenType = $$props.specimenType);
    		if ("specimenId" in $$props) $$invalidate(1, specimenId = $$props.specimenId);
    	};

    	return [specimenType, specimenId];
    }

    class SuggestionForm extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, { specimenType: 0, specimenId: 1 });
    	}
    }

    /* src/components/client/CommentThread.svelte generated by Svelte v3.23.0 */

    function add_css$1() {
    	var style = element("style");
    	style.id = "svelte-1qq3lax-style";
    	style.textContent = ".notification-switch.svelte-1qq3lax.svelte-1qq3lax{display:flex;justify-content:center;align-items:center}.notification-switch.svelte-1qq3lax>.svelte-1qq3lax{margin-left:.5rem;margin-right:.5rem}.notification-switch.svelte-1qq3lax button.svelte-1qq3lax{display:inline-block;font-size:0}.toggle.-active.svelte-1qq3lax .switch.svelte-1qq3lax{transform:translateX(13px);fill:#fff;transition:fill .2s linear}.toggle.-active.svelte-1qq3lax .bg.svelte-1qq3lax{fill:#000;transition:fill .2s linear}";
    	append(document.head, style);
    }

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[14] = list[i];
    	return child_ctx;
    }

    // (38:2) {:else}
    function create_else_block_2(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");
    			div.textContent = "There are no comments yet...";
    			attr(div, "class", "intro _text-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (11:2) {#if specimenData.comments && specimenData.comments.length}
    function create_if_block_3(ctx) {
    	let each_1_anchor;
    	let each_value = /*specimenData*/ ctx[5].comments;
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	return {
    		c() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*specimenData*/ 32) {
    				each_value = /*specimenData*/ ctx[5].comments;
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		d(detaching) {
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach(each_1_anchor);
    		}
    	};
    }

    // (29:6) {:else}
    function create_else_block_1(ctx) {
    	let div1;
    	let div0;
    	let strong;
    	let t0_value = /*comment*/ ctx[14].user + "";
    	let t0;
    	let t1;
    	let t2;
    	let html_tag;
    	let raw_value = /*comment*/ ctx[14].body + "";
    	let t3;

    	return {
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			strong = element("strong");
    			t0 = text(t0_value);
    			t1 = text(" says:");
    			t2 = space();
    			t3 = space();
    			attr(div0, "class", "user _spaced-b-min");
    			html_tag = new HtmlTag(t3);
    			attr(div1, "class", "comment -default _padded");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div0, strong);
    			append(strong, t0);
    			append(strong, t1);
    			append(div1, t2);
    			html_tag.m(raw_value, div1);
    			append(div1, t3);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*specimenData*/ 32 && t0_value !== (t0_value = /*comment*/ ctx[14].user + "")) set_data(t0, t0_value);
    			if (dirty & /*specimenData*/ 32 && raw_value !== (raw_value = /*comment*/ ctx[14].body + "")) html_tag.p(raw_value);
    		},
    		d(detaching) {
    			if (detaching) detach(div1);
    		}
    	};
    }

    // (25:39) 
    function create_if_block_7(ctx) {
    	let div;
    	let strong;
    	let t0_value = /*comment*/ ctx[14].user + "";
    	let t0;
    	let t1;
    	let a;
    	let t2_value = /*comment*/ ctx[14].body + "";
    	let t2;
    	let a_href_value;
    	let t3;

    	return {
    		c() {
    			div = element("div");
    			strong = element("strong");
    			t0 = text(t0_value);
    			t1 = text(" identified this as ");
    			a = element("a");
    			t2 = text(t2_value);
    			t3 = space();
    			attr(a, "href", a_href_value = /*comment*/ ctx[14].url);
    			attr(a, "target", "_blank");
    			attr(div, "class", "comment -font _padded _spaced-min _text-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, strong);
    			append(strong, t0);
    			append(div, t1);
    			append(div, a);
    			append(a, t2);
    			append(div, t3);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*specimenData*/ 32 && t0_value !== (t0_value = /*comment*/ ctx[14].user + "")) set_data(t0, t0_value);
    			if (dirty & /*specimenData*/ 32 && t2_value !== (t2_value = /*comment*/ ctx[14].body + "")) set_data(t2, t2_value);

    			if (dirty & /*specimenData*/ 32 && a_href_value !== (a_href_value = /*comment*/ ctx[14].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (21:40) 
    function create_if_block_6(ctx) {
    	let div;
    	let strong;
    	let t0_value = /*comment*/ ctx[14].user + "";
    	let t0;
    	let t1;
    	let a;
    	let t2_value = /*comment*/ ctx[14].body + "";
    	let t2;
    	let a_href_value;
    	let t3;

    	return {
    		c() {
    			div = element("div");
    			strong = element("strong");
    			t0 = text(t0_value);
    			t1 = text(" suggested this could be ");
    			a = element("a");
    			t2 = text(t2_value);
    			t3 = space();
    			attr(a, "href", a_href_value = /*comment*/ ctx[14].url);
    			attr(a, "target", "_blank");
    			attr(div, "class", "comment -match _padded _spaced-min _text-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, strong);
    			append(strong, t0);
    			append(div, t1);
    			append(div, a);
    			append(a, t2);
    			append(div, t3);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*specimenData*/ 32 && t0_value !== (t0_value = /*comment*/ ctx[14].user + "")) set_data(t0, t0_value);
    			if (dirty & /*specimenData*/ 32 && t2_value !== (t2_value = /*comment*/ ctx[14].body + "")) set_data(t2, t2_value);

    			if (dirty & /*specimenData*/ 32 && a_href_value !== (a_href_value = /*comment*/ ctx[14].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (17:42) 
    function create_if_block_5(ctx) {
    	let div;
    	let strong;
    	let t0_value = /*comment*/ ctx[14].user + "";
    	let t0;
    	let t1;
    	let a;
    	let t2_value = /*comment*/ ctx[14].body + "";
    	let t2;
    	let a_href_value;
    	let t3;

    	return {
    		c() {
    			div = element("div");
    			strong = element("strong");
    			t0 = text(t0_value);
    			t1 = text(" suggested this is similar to ");
    			a = element("a");
    			t2 = text(t2_value);
    			t3 = space();
    			attr(a, "href", a_href_value = /*comment*/ ctx[14].url);
    			attr(a, "target", "_blank");
    			attr(div, "class", "comment -similar _padded _spaced-min _text-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, strong);
    			append(strong, t0);
    			append(div, t1);
    			append(div, a);
    			append(a, t2);
    			append(div, t3);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*specimenData*/ 32 && t0_value !== (t0_value = /*comment*/ ctx[14].user + "")) set_data(t0, t0_value);
    			if (dirty & /*specimenData*/ 32 && t2_value !== (t2_value = /*comment*/ ctx[14].body + "")) set_data(t2, t2_value);

    			if (dirty & /*specimenData*/ 32 && a_href_value !== (a_href_value = /*comment*/ ctx[14].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (13:6) {#if comment.type == 'hand'}
    function create_if_block_4(ctx) {
    	let div;
    	let strong;
    	let t0_value = /*comment*/ ctx[14].user + "";
    	let t0;
    	let t1;

    	return {
    		c() {
    			div = element("div");
    			strong = element("strong");
    			t0 = text(t0_value);
    			t1 = text(" suggested this contains hand lettering\n        ");
    			attr(div, "class", "comment -similar _padded _spaced-min _text-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, strong);
    			append(strong, t0);
    			append(div, t1);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*specimenData*/ 32 && t0_value !== (t0_value = /*comment*/ ctx[14].user + "")) set_data(t0, t0_value);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (12:4) {#each specimenData.comments as comment}
    function create_each_block(ctx) {
    	let if_block_anchor;

    	function select_block_type_1(ctx, dirty) {
    		if (/*comment*/ ctx[14].type == "hand") return create_if_block_4;
    		if (/*comment*/ ctx[14].type == "similar") return create_if_block_5;
    		if (/*comment*/ ctx[14].type == "match") return create_if_block_6;
    		if (/*comment*/ ctx[14].type == "font") return create_if_block_7;
    		return create_else_block_1;
    	}

    	let current_block_type = select_block_type_1(ctx);
    	let if_block = current_block_type(ctx);

    	return {
    		c() {
    			if_block.c();
    			if_block_anchor = empty();
    		},
    		m(target, anchor) {
    			if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (current_block_type === (current_block_type = select_block_type_1(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			}
    		},
    		d(detaching) {
    			if_block.d(detaching);
    			if (detaching) detach(if_block_anchor);
    		}
    	};
    }

    // (42:2) {#if specimenData.name == "Handwritten"}
    function create_if_block_2(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");

    			div.innerHTML = `<strong>simon</strong> identified this as handwritten type
    `;

    			attr(div, "class", "comment -font _padded _spaced-min _text-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (67:2) {:else}
    function create_else_block$1(ctx) {
    	let div;
    	let a0;
    	let t0;
    	let a0_href_value;
    	let t1;
    	let a1;
    	let t2;
    	let a1_href_value;
    	let t3;

    	return {
    		c() {
    			div = element("div");
    			a0 = element("a");
    			t0 = text("Register");
    			t1 = text(" or ");
    			a1 = element("a");
    			t2 = text("login");
    			t3 = text(" to comment");

    			attr(a0, "href", a0_href_value = "/register" + (/*returnToUrl*/ ctx[6]
    			? "?referrer=" + /*returnToUrl*/ ctx[6]
    			: ""));

    			attr(a1, "href", a1_href_value = "/login" + (/*returnToUrl*/ ctx[6]
    			? "?referrer=" + /*returnToUrl*/ ctx[6]
    			: ""));

    			attr(div, "class", "_spaced-med _text-center");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, a0);
    			append(a0, t0);
    			append(div, t1);
    			append(div, a1);
    			append(a1, t2);
    			append(div, t3);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*returnToUrl*/ 64 && a0_href_value !== (a0_href_value = "/register" + (/*returnToUrl*/ ctx[6]
    			? "?referrer=" + /*returnToUrl*/ ctx[6]
    			: ""))) {
    				attr(a0, "href", a0_href_value);
    			}

    			if (dirty & /*returnToUrl*/ 64 && a1_href_value !== (a1_href_value = "/login" + (/*returnToUrl*/ ctx[6]
    			? "?referrer=" + /*returnToUrl*/ ctx[6]
    			: ""))) {
    				attr(a1, "href", a1_href_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (63:45) 
    function create_if_block_1$1(ctx) {
    	let current;

    	const commentform = new CommentForm({
    			props: {
    				specimenId: /*specimenId*/ ctx[3],
    				specimenType: /*context*/ ctx[4]
    			}
    		});

    	return {
    		c() {
    			create_component(commentform.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(commentform, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const commentform_changes = {};
    			if (dirty & /*specimenId*/ 8) commentform_changes.specimenId = /*specimenId*/ ctx[3];
    			if (dirty & /*context*/ 16) commentform_changes.specimenType = /*context*/ ctx[4];
    			commentform.$set(commentform_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(commentform.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(commentform.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(commentform, detaching);
    		}
    	};
    }

    // (48:2) {#if user && context == "specimens"}
    function create_if_block$1(ctx) {
    	let div0;
    	let button0;
    	let t0;
    	let button0_class_value;
    	let t1;
    	let button1;
    	let t2;
    	let button1_class_value;
    	let t3;
    	let div1;
    	let div1_class_value;
    	let t4;
    	let div2;
    	let div2_class_value;
    	let current;
    	let mounted;
    	let dispose;

    	const commentform = new CommentForm({
    			props: {
    				specimenId: /*specimenId*/ ctx[3],
    				specimenType: /*context*/ ctx[4]
    			}
    		});

    	const suggestionform = new SuggestionForm({
    			props: {
    				specimenId: /*specimenId*/ ctx[3],
    				specimenType: /*context*/ ctx[4]
    			}
    		});

    	return {
    		c() {
    			div0 = element("div");
    			button0 = element("button");
    			t0 = text("Comment");
    			t1 = space();
    			button1 = element("button");
    			t2 = text("Make a suggestion");
    			t3 = space();
    			div1 = element("div");
    			create_component(commentform.$$.fragment);
    			t4 = space();
    			div2 = element("div");
    			create_component(suggestionform.$$.fragment);
    			attr(button0, "class", button0_class_value = "ui-button -outline " + (/*activeTab*/ ctx[0] == "comment" ? "-active" : ""));
    			attr(button1, "class", button1_class_value = "ui-button -outline " + (/*activeTab*/ ctx[0] == "suggestion" ? "-active" : ""));
    			attr(div0, "class", "ui-tabs _spaced-t-med");
    			attr(div1, "class", div1_class_value = "tab-section " + (/*activeTab*/ ctx[0] == "comment" ? "-active" : ""));
    			attr(div2, "class", div2_class_value = "tab-section " + (/*activeTab*/ ctx[0] == "suggestion" ? "-active" : ""));
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			append(div0, button0);
    			append(button0, t0);
    			append(div0, t1);
    			append(div0, button1);
    			append(button1, t2);
    			insert(target, t3, anchor);
    			insert(target, div1, anchor);
    			mount_component(commentform, div1, null);
    			insert(target, t4, anchor);
    			insert(target, div2, anchor);
    			mount_component(suggestionform, div2, null);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen(button0, "click", /*click_handler_1*/ ctx[12]),
    					listen(button1, "click", /*click_handler_2*/ ctx[13])
    				];

    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (!current || dirty & /*activeTab*/ 1 && button0_class_value !== (button0_class_value = "ui-button -outline " + (/*activeTab*/ ctx[0] == "comment" ? "-active" : ""))) {
    				attr(button0, "class", button0_class_value);
    			}

    			if (!current || dirty & /*activeTab*/ 1 && button1_class_value !== (button1_class_value = "ui-button -outline " + (/*activeTab*/ ctx[0] == "suggestion" ? "-active" : ""))) {
    				attr(button1, "class", button1_class_value);
    			}

    			const commentform_changes = {};
    			if (dirty & /*specimenId*/ 8) commentform_changes.specimenId = /*specimenId*/ ctx[3];
    			if (dirty & /*context*/ 16) commentform_changes.specimenType = /*context*/ ctx[4];
    			commentform.$set(commentform_changes);

    			if (!current || dirty & /*activeTab*/ 1 && div1_class_value !== (div1_class_value = "tab-section " + (/*activeTab*/ ctx[0] == "comment" ? "-active" : ""))) {
    				attr(div1, "class", div1_class_value);
    			}

    			const suggestionform_changes = {};
    			if (dirty & /*specimenId*/ 8) suggestionform_changes.specimenId = /*specimenId*/ ctx[3];
    			if (dirty & /*context*/ 16) suggestionform_changes.specimenType = /*context*/ ctx[4];
    			suggestionform.$set(suggestionform_changes);

    			if (!current || dirty & /*activeTab*/ 1 && div2_class_value !== (div2_class_value = "tab-section " + (/*activeTab*/ ctx[0] == "suggestion" ? "-active" : ""))) {
    				attr(div2, "class", div2_class_value);
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(commentform.$$.fragment, local);
    			transition_in(suggestionform.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(commentform.$$.fragment, local);
    			transition_out(suggestionform.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div0);
    			if (detaching) detach(t3);
    			if (detaching) detach(div1);
    			destroy_component(commentform);
    			if (detaching) detach(t4);
    			if (detaching) detach(div2);
    			destroy_component(suggestionform);
    			mounted = false;
    			run_all(dispose);
    		}
    	};
    }

    function create_fragment$4(ctx) {
    	let div1;
    	let h2;
    	let t1;
    	let div0;
    	let t2;
    	let button;
    	let svg;
    	let path0;
    	let clipPath;
    	let path1;
    	let g;
    	let path2;
    	let path3;
    	let path4;
    	let svg_class_value;
    	let t3;
    	let t4;
    	let t5;
    	let current_block_type_index;
    	let if_block2;
    	let current;
    	let mounted;
    	let dispose;

    	function select_block_type(ctx, dirty) {
    		if (/*specimenData*/ ctx[5].comments && /*specimenData*/ ctx[5].comments.length) return create_if_block_3;
    		return create_else_block_2;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = /*specimenData*/ ctx[5].name == "Handwritten" && create_if_block_2();
    	const if_block_creators = [create_if_block$1, create_if_block_1$1, create_else_block$1];
    	const if_blocks = [];

    	function select_block_type_2(ctx, dirty) {
    		if (/*user*/ ctx[2] && /*context*/ ctx[4] == "specimens") return 0;
    		if (/*user*/ ctx[2] && /*context*/ ctx[4] == "inspiration") return 1;
    		return 2;
    	}

    	current_block_type_index = select_block_type_2(ctx);
    	if_block2 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);

    	return {
    		c() {
    			div1 = element("div");
    			h2 = element("h2");
    			h2.textContent = "Comments";
    			t1 = space();
    			div0 = element("div");
    			t2 = text("Get notified of updates\n      ");
    			button = element("button");
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			clipPath = svg_element("clipPath");
    			path1 = svg_element("path");
    			g = svg_element("g");
    			path2 = svg_element("path");
    			path3 = svg_element("path");
    			path4 = svg_element("path");
    			t3 = space();
    			if_block0.c();
    			t4 = space();
    			if (if_block1) if_block1.c();
    			t5 = space();
    			if_block2.c();
    			attr(h2, "class", "_text-center");
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h35v22.273H0z");
    			attr(path1, "d", "M0 0h35v22.273H0z");
    			attr(clipPath, "id", "a");
    			attr(path2, "d", "M23.864 22.273H11.136C5.027 22.273 0 17.246 0 11.136 0 5.076 4.948.08 11.136 0h12.728C29.973 0 35 5.027 35 11.136c0 6.109-5.027 11.137-11.136 11.137zm0-19.091H11.136c-4.364 0-7.954 3.591-7.954 7.954 0 4.364 3.591 7.955 7.954 7.955h12.728c4.363 0 7.954-3.591 7.954-7.955 0-4.363-3.59-7.954-7.954-7.954z");
    			attr(path3, "class", "bg svelte-1qq3lax");
    			attr(path3, "d", "M23.864 3.182H11.136c-4.363 0-7.954 3.591-7.954 7.954 0 4.364 3.591 7.955 7.954 7.955h12.728c4.363 0 7.954-3.591 7.954-7.955 0-4.363-3.591-7.954-7.954-7.954z");
    			attr(path3, "fill", "none");
    			attr(path4, "class", "switch svelte-1qq3lax");
    			attr(path4, "d", "M11.136 15.909c-2.618 0-4.772-2.154-4.772-4.773 0-2.618 2.154-4.772 4.772-4.772 2.619 0 4.773 2.154 4.773 4.772 0 2.569-2.074 4.692-4.773 4.773z");
    			attr(g, "clip-path", "url(#a)");
    			attr(svg, "class", svg_class_value = "toggle " + (/*userIsSubscribed*/ ctx[1] ? "-active" : "") + " svelte-1qq3lax");
    			attr(svg, "width", "35");
    			attr(svg, "height", "22.3");
    			attr(svg, "viewBox", "0 0 35 23");
    			attr(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr(svg, "fill-rule", "evenodd");
    			attr(svg, "clip-rule", "evenodd");
    			attr(svg, "stroke-linejoin", "round");
    			attr(svg, "stroke-miterlimit", "2");
    			attr(button, "class", "ui-button -unstyled svelte-1qq3lax");
    			attr(div0, "class", "notification-switch _spaced-tb-min svelte-1qq3lax");
    			attr(div1, "class", "ui-container -slim _spaced-med _padded-lr-min svelte-1qq3lax");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, h2);
    			append(div1, t1);
    			append(div1, div0);
    			append(div0, t2);
    			append(div0, button);
    			append(button, svg);
    			append(svg, path0);
    			append(svg, clipPath);
    			append(clipPath, path1);
    			append(svg, g);
    			append(g, path2);
    			append(g, path3);
    			append(g, path4);
    			append(div1, t3);
    			if_block0.m(div1, null);
    			append(div1, t4);
    			if (if_block1) if_block1.m(div1, null);
    			append(div1, t5);
    			if_blocks[current_block_type_index].m(div1, null);
    			current = true;

    			if (!mounted) {
    				dispose = listen(button, "click", /*click_handler*/ ctx[11]);
    				mounted = true;
    			}
    		},
    		p(ctx, [dirty]) {
    			if (!current || dirty & /*userIsSubscribed*/ 2 && svg_class_value !== (svg_class_value = "toggle " + (/*userIsSubscribed*/ ctx[1] ? "-active" : "") + " svelte-1qq3lax")) {
    				attr(svg, "class", svg_class_value);
    			}

    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block0) {
    				if_block0.p(ctx, dirty);
    			} else {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(div1, t4);
    				}
    			}

    			if (/*specimenData*/ ctx[5].name == "Handwritten") {
    				if (if_block1) ; else {
    					if_block1 = create_if_block_2();
    					if_block1.c();
    					if_block1.m(div1, t5);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type_2(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if_blocks[current_block_type_index].p(ctx, dirty);
    			} else {
    				group_outros();

    				transition_out(if_blocks[previous_block_index], 1, 1, () => {
    					if_blocks[previous_block_index] = null;
    				});

    				check_outros();
    				if_block2 = if_blocks[current_block_type_index];

    				if (!if_block2) {
    					if_block2 = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    					if_block2.c();
    				}

    				transition_in(if_block2, 1);
    				if_block2.m(div1, null);
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(if_block2);
    			current = true;
    		},
    		o(local) {
    			transition_out(if_block2);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div1);
    			if_block0.d();
    			if (if_block1) if_block1.d();
    			if_blocks[current_block_type_index].d();
    			mounted = false;
    			dispose();
    		}
    	};
    }

    function instance$4($$self, $$props, $$invalidate) {
    	const dispatch = createEventDispatcher();

    	let { activeTab = "comment" } = $$props,
    		{ user = globalUser } = $$props,
    		{ specimenId = pageId } = $$props,
    		{ context = page === "specimen" ? "specimens" : "inspiration" } = $$props,
    		{ specimenData = globalData } = $$props,
    		{ subscribers = specimenData.subscribers
    		? specimenData.subscribers
    		: false } = $$props,
    		{ returnToUrl } = $$props,
    		{ userIsSubscribed = user != "" && subscribers && specimenData.subscribers.indexOf(user) != -1
    		? true
    		: false } = $$props;

    	function switchTab(e) {
    		if (e.target.classList.contains("-active")) return;

    		if (activeTab == "comment") {
    			$$invalidate(0, activeTab = "suggestion");
    		} else {
    			$$invalidate(0, activeTab = "comment");
    		}
    	}

    	function handleSubscribe(user, subscribe) {
    		if (!user) {
    			dispatch("fireLightbox", `<div class="ui-heading -h3">Sign up for notifications</div><a href="/register?referrer=${returnToUrl}">Register</a> or <a href="/login?referrer=${returnToUrl}">login</a> to add specimens, comments and suggestions.`);
    		} else {
    			const xhr = new XMLHttpRequest();
    			xhr.open("POST", `/${page == "specimen" ? "specimen" : "feature"}/toggleSubscriber`);

    			// TODO This handling of terminology needs to be cleaned up
    			let params = `specimen=${specimenId}&user=${user}&subscribed=${userIsSubscribed}`;

    			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    			xhr.send(params);

    			xhr.onload = () => {
    				$$invalidate(1, userIsSubscribed = !userIsSubscribed);
    			};
    		}
    	}

    	const click_handler = () => handleSubscribe(user);
    	const click_handler_1 = e => switchTab(e);
    	const click_handler_2 = e => switchTab(e);

    	$$self.$set = $$props => {
    		if ("activeTab" in $$props) $$invalidate(0, activeTab = $$props.activeTab);
    		if ("user" in $$props) $$invalidate(2, user = $$props.user);
    		if ("specimenId" in $$props) $$invalidate(3, specimenId = $$props.specimenId);
    		if ("context" in $$props) $$invalidate(4, context = $$props.context);
    		if ("specimenData" in $$props) $$invalidate(5, specimenData = $$props.specimenData);
    		if ("subscribers" in $$props) $$invalidate(9, subscribers = $$props.subscribers);
    		if ("returnToUrl" in $$props) $$invalidate(6, returnToUrl = $$props.returnToUrl);
    		if ("userIsSubscribed" in $$props) $$invalidate(1, userIsSubscribed = $$props.userIsSubscribed);
    	};

    	return [
    		activeTab,
    		userIsSubscribed,
    		user,
    		specimenId,
    		context,
    		specimenData,
    		returnToUrl,
    		switchTab,
    		handleSubscribe,
    		subscribers,
    		dispatch,
    		click_handler,
    		click_handler_1,
    		click_handler_2
    	];
    }

    class CommentThread extends SvelteComponent {
    	constructor(options) {
    		super();
    		if (!document.getElementById("svelte-1qq3lax-style")) add_css$1();

    		init(this, options, instance$4, create_fragment$4, safe_not_equal, {
    			activeTab: 0,
    			user: 2,
    			specimenId: 3,
    			context: 4,
    			specimenData: 5,
    			subscribers: 9,
    			returnToUrl: 6,
    			userIsSubscribed: 1
    		});
    	}
    }

    /* src/components/client/SubmissionForm.svelte generated by Svelte v3.23.0 */

    const { document: document_1$1 } = globals;

    function add_css$2() {
    	var style = element("style");
    	style.id = "svelte-11i41vl-style";
    	style.textContent = ".step.svelte-11i41vl{display:none}.step.-active.svelte-11i41vl{display:block}";
    	append(document_1$1.head, style);
    }

    function create_fragment$5(ctx) {
    	let div0;
    	let t0;
    	let form;
    	let div2;
    	let h1;
    	let t2;
    	let p;
    	let t12;
    	let label0;
    	let t14;
    	let input0;
    	let t15;
    	let div1;
    	let div2_class_value;
    	let t16;
    	let div4;
    	let div3;
    	let input1;
    	let t17;
    	let label1;
    	let div3_class_value;
    	let div4_class_value;
    	let t19;
    	let div12;
    	let div5;
    	let t21;
    	let div6;
    	let t24;
    	let div7;
    	let t27;
    	let div8;
    	let t29;
    	let div9;
    	let t32;
    	let div11;
    	let div12_class_value;
    	let t37;
    	let div13;
    	let button;
    	let span;
    	let t39;
    	let svg;
    	let path0;
    	let path1;
    	let button_class_value;
    	let div13_class_value;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			div0 = element("div");
    			t0 = space();
    			form = element("form");
    			div2 = element("div");
    			h1 = element("h1");
    			h1.textContent = "Upload a specimen to the reel";
    			t2 = space();
    			p = element("p");
    			p.innerHTML = `Currently supported formats are <strong>.jpg</strong>, <strong>.png</strong> and <strong>.gif</strong> with a max file size of <strong>5MB</strong>.`;
    			t12 = space();
    			label0 = element("label");
    			label0.textContent = "Select a file...";
    			t14 = space();
    			input0 = element("input");
    			t15 = space();
    			div1 = element("div");
    			t16 = space();
    			div4 = element("div");
    			div3 = element("div");
    			input1 = element("input");
    			t17 = space();
    			label1 = element("label");
    			label1.textContent = "I confirm that I own this image or have permission to upload it";
    			t19 = space();
    			div12 = element("div");
    			div5 = element("div");
    			div5.textContent = "If you found this image online please credit the source";
    			t21 = space();
    			div6 = element("div");

    			div6.innerHTML = `<label for="creditName" class="label">Image credit name</label> 
      <input type="text" name="creditName" class="input -text">`;

    			t24 = space();
    			div7 = element("div");

    			div7.innerHTML = `<label for="creditUrl" class="label">Image credit URL</label> 
      <input type="text" name="creditUrl" class="input -text" placeholder="e.g. www.reddit.com/...">`;

    			t27 = space();
    			div8 = element("div");
    			div8.textContent = "If you know details about the type in this image please add them here, otherwise leave these fields blank";
    			t29 = space();
    			div9 = element("div");

    			div9.innerHTML = `<label for="name" class="label">Font name</label> 
      <input type="text" name="name" class="input -text" placeholder="e.g. Helvetica Nueue">`;

    			t32 = space();
    			div11 = element("div");

    			div11.innerHTML = `<label for="reference" class="label">Reference link</label> 
      <input type="text" name="reference" class="input -text" placeholder="http://"> 
      <div class="hint _spaced-min"><small>A reference link is the location of where the font is available online. It helps us to confirm the id of a font.</small></div>`;

    			t37 = space();
    			div13 = element("div");
    			button = element("button");
    			span = element("span");
    			span.textContent = "Submit";
    			t39 = space();
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			attr(div0, "id", "preview");
    			attr(div0, "class", "_spaced-t-med");
    			attr(label0, "for", "specimenFile");
    			attr(label0, "class", "label -file");
    			attr(label0, "id", "specimenLabel");
    			attr(input0, "type", "file");
    			attr(input0, "name", "specimenFile");
    			attr(input0, "id", "specimenFile");
    			attr(input0, "class", "input -file");
    			attr(div1, "class", "validation");
    			attr(div2, "class", div2_class_value = "form-group step " + (/*step*/ ctx[0] == 1 ? "-active" : "") + " svelte-11i41vl");
    			attr(input1, "type", "checkbox");
    			attr(input1, "name", "consent");
    			attr(input1, "id", "consent");
    			attr(input1, "class", "input -checkbox");
    			input1.required = true;
    			attr(label1, "for", "consent");
    			attr(label1, "class", "_slight");
    			attr(div3, "class", div3_class_value = "highlightConsent " + (/*highlightConsent*/ ctx[1] ? "-active" : ""));
    			attr(div4, "class", div4_class_value = "form-group step " + (/*step*/ ctx[0] == 2 ? "-active" : "") + " svelte-11i41vl");
    			attr(div6, "class", "form-group _spaced-min");
    			attr(div7, "class", "form-group _spaced-min");
    			attr(div9, "class", "form-group _spaced-min");
    			attr(div11, "class", "form-group _spaced-min");
    			attr(div12, "class", div12_class_value = "ui-panel -bg _padded _spaced-min step " + (/*step*/ ctx[0] == 2 ? "-active" : "") + " svelte-11i41vl");
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path1, "id", "spinner");
    			attr(path1, "d", "M18.364 5.636L16.95 7.05A7 7 0 1 0 19 12h2a9 9 0 1 1-2.636-6.364z");
    			attr(svg, "fill", "#fff");
    			attr(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr(svg, "viewBox", "0 0 24 24");
    			attr(svg, "width", "24");
    			attr(svg, "height", "24");
    			attr(button, "id", "submissionSubmit");
    			attr(button, "class", button_class_value = /*loading*/ ctx[2] ? "loading" : "");
    			attr(div13, "class", div13_class_value = "_spaced-med step " + (/*step*/ ctx[0] == 2 ? "-active" : "") + " svelte-11i41vl");
    			attr(form, "action", "/upload");
    			attr(form, "method", "post");
    			attr(form, "enctype", "multipart/form-data");
    			attr(form, "autocomplete", "off");
    			attr(form, "class", "ui-form _spaced-med");
    			attr(form, "id", "specimenSubmissionForm");
    		},
    		m(target, anchor) {
    			insert(target, div0, anchor);
    			insert(target, t0, anchor);
    			insert(target, form, anchor);
    			append(form, div2);
    			append(div2, h1);
    			append(div2, t2);
    			append(div2, p);
    			append(div2, t12);
    			append(div2, label0);
    			append(div2, t14);
    			append(div2, input0);
    			append(div2, t15);
    			append(div2, div1);
    			append(form, t16);
    			append(form, div4);
    			append(div4, div3);
    			append(div3, input1);
    			append(div3, t17);
    			append(div3, label1);
    			append(form, t19);
    			append(form, div12);
    			append(div12, div5);
    			append(div12, t21);
    			append(div12, div6);
    			append(div12, t24);
    			append(div12, div7);
    			append(div12, t27);
    			append(div12, div8);
    			append(div12, t29);
    			append(div12, div9);
    			append(div12, t32);
    			append(div12, div11);
    			append(form, t37);
    			append(form, div13);
    			append(div13, button);
    			append(button, span);
    			append(button, t39);
    			append(button, svg);
    			append(svg, path0);
    			append(svg, path1);

    			if (!mounted) {
    				dispose = [
    					listen(input0, "change", /*change_handler*/ ctx[5]),
    					listen(button, "click", /*submitSubmissionForm*/ ctx[4])
    				];

    				mounted = true;
    			}
    		},
    		p(ctx, [dirty]) {
    			if (dirty & /*step*/ 1 && div2_class_value !== (div2_class_value = "form-group step " + (/*step*/ ctx[0] == 1 ? "-active" : "") + " svelte-11i41vl")) {
    				attr(div2, "class", div2_class_value);
    			}

    			if (dirty & /*highlightConsent*/ 2 && div3_class_value !== (div3_class_value = "highlightConsent " + (/*highlightConsent*/ ctx[1] ? "-active" : ""))) {
    				attr(div3, "class", div3_class_value);
    			}

    			if (dirty & /*step*/ 1 && div4_class_value !== (div4_class_value = "form-group step " + (/*step*/ ctx[0] == 2 ? "-active" : "") + " svelte-11i41vl")) {
    				attr(div4, "class", div4_class_value);
    			}

    			if (dirty & /*step*/ 1 && div12_class_value !== (div12_class_value = "ui-panel -bg _padded _spaced-min step " + (/*step*/ ctx[0] == 2 ? "-active" : "") + " svelte-11i41vl")) {
    				attr(div12, "class", div12_class_value);
    			}

    			if (dirty & /*loading*/ 4 && button_class_value !== (button_class_value = /*loading*/ ctx[2] ? "loading" : "")) {
    				attr(button, "class", button_class_value);
    			}

    			if (dirty & /*step*/ 1 && div13_class_value !== (div13_class_value = "_spaced-med step " + (/*step*/ ctx[0] == 2 ? "-active" : "") + " svelte-11i41vl")) {
    				attr(div13, "class", div13_class_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(div0);
    			if (detaching) detach(t0);
    			if (detaching) detach(form);
    			mounted = false;
    			run_all(dispose);
    		}
    	};
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let { step = 1 } = $$props;
    	let { highlightConsent } = $$props;
    	let loading = false;

    	function handleFileSelect(e) {
    		var files = e.target.files;
    		var f = files[0];
    		var reader = new FileReader();

    		reader.onload = (function (theFile) {
    			console.log(theFile.type == "image/jpeg");
    			const validationMessageContainer = document.querySelector(".validation");
    			validationMessageContainer.innerText = "";
    			const acceptedFileTypes = ["image/jpeg", "image/png", "image/gif"];

    			if (!acceptedFileTypes.includes(theFile.type)) {
    				validationMessageContainer.innerText = "Please select a file that matches the supported file types.";
    				return;
    			}

    			return function (e) {
    				document.getElementById("preview").innerHTML = ["<img src=\"", e.target.result, "\" title=\"", theFile.name, "\" />"].join("");
    				$$invalidate(0, step = 2);
    			};
    		})(f);

    		reader.readAsDataURL(f);
    	}

    	function submitSubmissionForm() {
    		const consentCheckbox = document.getElementById("consent");

    		if (consentCheckbox.checked) {
    			$$invalidate(2, loading = true);
    			document.getElementById("specimenSubmissionForm").submit();
    		}
    	}

    	const change_handler = e => handleFileSelect(e);

    	$$self.$set = $$props => {
    		if ("step" in $$props) $$invalidate(0, step = $$props.step);
    		if ("highlightConsent" in $$props) $$invalidate(1, highlightConsent = $$props.highlightConsent);
    	};

    	return [
    		step,
    		highlightConsent,
    		loading,
    		handleFileSelect,
    		submitSubmissionForm,
    		change_handler
    	];
    }

    class SubmissionForm extends SvelteComponent {
    	constructor(options) {
    		super();
    		if (!document_1$1.getElementById("svelte-11i41vl-style")) add_css$2();
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, { step: 0, highlightConsent: 1 });
    	}
    }

    /* src/components/client/SuggestionTags.svelte generated by Svelte v3.23.0 */

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[2] = list[i];
    	return child_ctx;
    }

    // (6:0) {#if hasSuggestions}
    function create_if_block_2$1(ctx) {
    	let h3;

    	return {
    		c() {
    			h3 = element("h3");
    			h3.textContent = "Suggestions";
    		},
    		m(target, anchor) {
    			insert(target, h3, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(h3);
    		}
    	};
    }

    // (12:4) {#if comment.type === 'similar' || comment.type === 'match'}
    function create_if_block_1$2(ctx) {
    	let a;
    	let t_value = /*comment*/ ctx[2].body + "";
    	let t;
    	let a_href_value;

    	return {
    		c() {
    			a = element("a");
    			t = text(t_value);
    			attr(a, "href", a_href_value = /*comment*/ ctx[2].url);
    			attr(a, "class", "ui-button -outline -small");
    		},
    		m(target, anchor) {
    			insert(target, a, anchor);
    			append(a, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*comments*/ 1 && t_value !== (t_value = /*comment*/ ctx[2].body + "")) set_data(t, t_value);

    			if (dirty & /*comments*/ 1 && a_href_value !== (a_href_value = /*comment*/ ctx[2].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(a);
    		}
    	};
    }

    // (15:4) {#if comment.type === 'hand'}
    function create_if_block$2(ctx) {
    	let div;

    	return {
    		c() {
    			div = element("div");
    			div.textContent = "Hand lettering";
    			attr(div, "class", "ui-tag -small");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (11:2) {#each comments as comment}
    function create_each_block$1(ctx) {
    	let t;
    	let if_block1_anchor;
    	let if_block0 = (/*comment*/ ctx[2].type === "similar" || /*comment*/ ctx[2].type === "match") && create_if_block_1$2(ctx);
    	let if_block1 = /*comment*/ ctx[2].type === "hand" && create_if_block$2();

    	return {
    		c() {
    			if (if_block0) if_block0.c();
    			t = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, if_block1_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (/*comment*/ ctx[2].type === "similar" || /*comment*/ ctx[2].type === "match") {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_1$2(ctx);
    					if_block0.c();
    					if_block0.m(t.parentNode, t);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*comment*/ ctx[2].type === "hand") {
    				if (if_block1) ; else {
    					if_block1 = create_if_block$2();
    					if_block1.c();
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		d(detaching) {
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach(t);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach(if_block1_anchor);
    		}
    	};
    }

    function create_fragment$6(ctx) {
    	let t;
    	let div;
    	let if_block = /*hasSuggestions*/ ctx[1] && create_if_block_2$1();
    	let each_value = /*comments*/ ctx[0];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	return {
    		c() {
    			if (if_block) if_block.c();
    			t = space();
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "ui-button-list");
    		},
    		m(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert(target, t, anchor);
    			insert(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p(ctx, [dirty]) {
    			if (/*hasSuggestions*/ ctx[1]) {
    				if (if_block) ; else {
    					if_block = create_if_block_2$1();
    					if_block.c();
    					if_block.m(t.parentNode, t);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}

    			if (dirty & /*comments*/ 1) {
    				each_value = /*comments*/ ctx[0];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach(t);
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    function instance$6($$self, $$props, $$invalidate) {
    	let { comments } = $$props;
    	let { hasSuggestions } = $$props;

    	$$self.$set = $$props => {
    		if ("comments" in $$props) $$invalidate(0, comments = $$props.comments);
    		if ("hasSuggestions" in $$props) $$invalidate(1, hasSuggestions = $$props.hasSuggestions);
    	};

    	return [comments, hasSuggestions];
    }

    class SuggestionTags extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, { comments: 0, hasSuggestions: 1 });
    	}
    }

    /* src/components/client/Showcase.svelte generated by Svelte v3.23.0 */

    const { document: document_1$2 } = globals;

    function add_css$3() {
    	var style = element("style");
    	style.id = "svelte-1is5jml-style";
    	style.textContent = ".ui-showcase.svelte-1is5jml.svelte-1is5jml{display:flex;flex-wrap:wrap;margin-top:2rem}.showcase-pane.svelte-1is5jml.svelte-1is5jml{width:100%;background:#eee}@media(min-width: 500px){.showcase-pane.svelte-1is5jml.svelte-1is5jml{width:50%}}.showcase-pane.image.svelte-1is5jml.svelte-1is5jml{display:flex;justify-content:flex-end;background:#ccc;text-align:center}.showcase-pane.meta.svelte-1is5jml.svelte-1is5jml{text-align:left}.pane.svelte-1is5jml.svelte-1is5jml{width:100%;max-width:600px}.pane.svelte-1is5jml>.title.svelte-1is5jml{font-size:1.8rem}#average-color.svelte-1is5jml.svelte-1is5jml{transition:background-color 0.5s linear}.item.svelte-1is5jml.svelte-1is5jml{margin-bottom:1rem}.title.svelte-1is5jml.svelte-1is5jml{display:flex;justify-content:space-between}.icon.svelte-1is5jml.svelte-1is5jml{cursor:pointer}";
    	append(document_1$2.head, style);
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[19] = list[i];
    	child_ctx[21] = i;
    	return child_ctx;
    }

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[16] = list[i];
    	return child_ctx;
    }

    function get_each_context_5(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[19] = list[i];
    	child_ctx[27] = i;
    	return child_ctx;
    }

    function get_each_context_4(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[19] = list[i];
    	child_ctx[27] = i;
    	return child_ctx;
    }

    function get_each_context_3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[24] = list[i];
    	child_ctx[21] = i;
    	return child_ctx;
    }

    function get_each_context_2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[16] = list[i];
    	child_ctx[23] = i;
    	return child_ctx;
    }

    function get_each_context_6(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[29] = list[i];
    	child_ctx[23] = i;
    	return child_ctx;
    }

    // (113:8) {#if pageType === 'feature'}
    function create_if_block_27(ctx) {
    	let svg;
    	let path;
    	let svg_class_value;
    	let svg_viewBox_value;

    	return {
    		c() {
    			svg = svg_element("svg");
    			path = svg_element("path");
    			attr(path, "id", "mask-path");
    			attr(path, "d", /*activePath*/ ctx[3]);
    			attr(path, "fill", "#000000");
    			attr(svg, "id", "hero-mask");
    			attr(svg, "class", svg_class_value = /*activePath*/ ctx[3] ? "-active" : "");
    			attr(svg, "viewBox", svg_viewBox_value = "0 0 " + /*data*/ ctx[0].image.width + "\n            " + /*data*/ ctx[0].image.height);
    			attr(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr(svg, "fill-rule", "evenodd");
    			attr(svg, "clip-rule", "evenodd");
    			attr(svg, "stroke-linejoin", "round");
    			attr(svg, "stroke-miterlimit", "2");
    		},
    		m(target, anchor) {
    			insert(target, svg, anchor);
    			append(svg, path);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*activePath*/ 8) {
    				attr(path, "d", /*activePath*/ ctx[3]);
    			}

    			if (dirty & /*activePath*/ 8 && svg_class_value !== (svg_class_value = /*activePath*/ ctx[3] ? "-active" : "")) {
    				attr(svg, "class", svg_class_value);
    			}

    			if (dirty & /*data*/ 1 && svg_viewBox_value !== (svg_viewBox_value = "0 0 " + /*data*/ ctx[0].image.width + "\n            " + /*data*/ ctx[0].image.height)) {
    				attr(svg, "viewBox", svg_viewBox_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(svg);
    		}
    	};
    }

    // (142:8) {:else}
    function create_else_block_7(ctx) {
    	let t;

    	return {
    		c() {
    			t = text("Unidentified Specimen");
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		p: noop,
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (139:8) {#if data.name}
    function create_if_block_25(ctx) {
    	let t0_value = /*data*/ ctx[0].name + "";
    	let t0;
    	let t1;
    	let if_block_anchor;
    	let if_block = /*data*/ ctx[0].name == "Handwritten" && create_if_block_26();

    	return {
    		c() {
    			t0 = text(t0_value);
    			t1 = space();
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		m(target, anchor) {
    			insert(target, t0, anchor);
    			insert(target, t1, anchor);
    			if (if_block) if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t0_value !== (t0_value = /*data*/ ctx[0].name + "")) set_data(t0, t0_value);

    			if (/*data*/ ctx[0].name == "Handwritten") {
    				if (if_block) ; else {
    					if_block = create_if_block_26();
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(t0);
    			if (detaching) detach(t1);
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach(if_block_anchor);
    		}
    	};
    }

    // (141:10) {#if data.name == 'Handwritten'}
    function create_if_block_26(ctx) {
    	let t;

    	return {
    		c() {
    			t = text("Type Specimen");
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (144:6) {#if pageType == 'specimen'}
    function create_if_block_24(ctx) {
    	let p;
    	let t0;
    	let t1_value = /*data*/ ctx[0].user + "";
    	let t1;

    	return {
    		c() {
    			p = element("p");
    			t0 = text("Uploaded by ");
    			t1 = text(t1_value);
    		},
    		m(target, anchor) {
    			insert(target, p, anchor);
    			append(p, t0);
    			append(p, t1);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t1_value !== (t1_value = /*data*/ ctx[0].user + "")) set_data(t1, t1_value);
    		},
    		d(detaching) {
    			if (detaching) detach(p);
    		}
    	};
    }

    // (147:6) {#if (data.credit && data.credit[0].name) || (data.credit && data.credit[0].url)}
    function create_if_block_22(ctx) {
    	let if_block_anchor;

    	function select_block_type_1(ctx, dirty) {
    		if (/*data*/ ctx[0].credit[0].name) return create_if_block_23;
    		return create_else_block_6;
    	}

    	let current_block_type = select_block_type_1(ctx);
    	let if_block = current_block_type(ctx);

    	return {
    		c() {
    			if_block.c();
    			if_block_anchor = empty();
    		},
    		m(target, anchor) {
    			if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (current_block_type === (current_block_type = select_block_type_1(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			}
    		},
    		d(detaching) {
    			if_block.d(detaching);
    			if (detaching) detach(if_block_anchor);
    		}
    	};
    }

    // (156:8) {:else}
    function create_else_block_6(ctx) {
    	let div;
    	let a;
    	let t;
    	let a_href_value;

    	return {
    		c() {
    			div = element("div");
    			a = element("a");
    			t = text("Image credit");
    			attr(a, "href", a_href_value = /*data*/ ctx[0].credit[0].url);
    			attr(a, "target", "_blank");
    			attr(div, "class", "item svelte-1is5jml");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, a);
    			append(a, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*data*/ ctx[0].credit[0].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    		}
    	};
    }

    // (148:8) {#if data.credit[0].name}
    function create_if_block_23(ctx) {
    	let div;
    	let t;
    	let each_value_6 = /*data*/ ctx[0].credit;
    	let each_blocks = [];

    	for (let i = 0; i < each_value_6.length; i += 1) {
    		each_blocks[i] = create_each_block_6(get_each_context_6(ctx, each_value_6, i));
    	}

    	return {
    		c() {
    			div = element("div");
    			t = text("Image credit:\n            ");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "item svelte-1is5jml");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1) {
    				each_value_6 = /*data*/ ctx[0].credit;
    				let i;

    				for (i = 0; i < each_value_6.length; i += 1) {
    					const child_ctx = get_each_context_6(ctx, each_value_6, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_6(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_6.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (151:12) {#each data.credit as credit, index}
    function create_each_block_6(ctx) {
    	let t0_value = (/*index*/ ctx[23] != 0 ? ", " : "") + "";
    	let t0;
    	let t1;
    	let a;
    	let t2_value = /*credit*/ ctx[29].name + "";
    	let t2;
    	let a_href_value;

    	return {
    		c() {
    			t0 = text(t0_value);
    			t1 = space();
    			a = element("a");
    			t2 = text(t2_value);
    			attr(a, "href", a_href_value = /*credit*/ ctx[29].url);
    			attr(a, "target", "_blank");
    		},
    		m(target, anchor) {
    			insert(target, t0, anchor);
    			insert(target, t1, anchor);
    			insert(target, a, anchor);
    			append(a, t2);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t2_value !== (t2_value = /*credit*/ ctx[29].name + "")) set_data(t2, t2_value);

    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*credit*/ ctx[29].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(t0);
    			if (detaching) detach(t1);
    			if (detaching) detach(a);
    		}
    	};
    }

    // (162:6) {#if data.identified && data.identified.length}
    function create_if_block_8(ctx) {
    	let h3;
    	let t1;
    	let each_1_anchor;
    	let each_value_2 = /*data*/ ctx[0].identified;
    	let each_blocks = [];

    	for (let i = 0; i < each_value_2.length; i += 1) {
    		each_blocks[i] = create_each_block_2(get_each_context_2(ctx, each_value_2, i));
    	}

    	return {
    		c() {
    			h3 = element("h3");
    			h3.textContent = "Type in this image";
    			t1 = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m(target, anchor) {
    			insert(target, h3, anchor);
    			insert(target, t1, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data, showSvg, hideSvg, toggleActivePathLock, activePathLock, activePath, pageType*/ 923) {
    				each_value_2 = /*data*/ ctx[0].identified;
    				let i;

    				for (i = 0; i < each_value_2.length; i += 1) {
    					const child_ctx = get_each_context_2(ctx, each_value_2, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_2(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_2.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(h3);
    			if (detaching) detach(t1);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach(each_1_anchor);
    		}
    	};
    }

    // (248:10) {:else}
    function create_else_block_4(ctx) {
    	let div2;
    	let div0;
    	let h3;
    	let t0_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].name + "";
    	let t0;
    	let t1;
    	let div1;
    	let t2;
    	let t3;
    	let t4;

    	function select_block_type_5(ctx, dirty) {
    		if (/*data*/ ctx[0].fonts[/*index*/ ctx[23]].url) return create_if_block_21;
    		return create_else_block_5;
    	}

    	let current_block_type = select_block_type_5(ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors && /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors.length && create_if_block_19(ctx);

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			h3 = element("h3");
    			t0 = text(t0_value);
    			t1 = space();
    			div1 = element("div");
    			t2 = text("Foundry/Designer:\n                ");
    			if_block0.c();
    			t3 = space();
    			if (if_block1) if_block1.c();
    			t4 = space();
    			attr(div0, "class", "title svelte-1is5jml");
    			attr(div1, "class", "_padded-tb-min");
    			attr(div2, "class", "font-card _padded-min");
    		},
    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, div0);
    			append(div0, h3);
    			append(h3, t0);
    			append(div2, t1);
    			append(div2, div1);
    			append(div1, t2);
    			if_block0.m(div1, null);
    			append(div2, t3);
    			if (if_block1) if_block1.m(div2, null);
    			append(div2, t4);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t0_value !== (t0_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].name + "")) set_data(t0, t0_value);

    			if (current_block_type === (current_block_type = select_block_type_5(ctx)) && if_block0) {
    				if_block0.p(ctx, dirty);
    			} else {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(div1, null);
    				}
    			}

    			if (/*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors && /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors.length) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_19(ctx);
    					if_block1.c();
    					if_block1.m(div2, t4);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div2);
    			if_block0.d();
    			if (if_block1) if_block1.d();
    		}
    	};
    }

    // (165:10) {#if font.versions}
    function create_if_block_9(ctx) {
    	let each_1_anchor;
    	let each_value_3 = /*font*/ ctx[16].versions;
    	let each_blocks = [];

    	for (let i = 0; i < each_value_3.length; i += 1) {
    		each_blocks[i] = create_each_block_3(get_each_context_3(ctx, each_value_3, i));
    	}

    	return {
    		c() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*showSvg, data, hideSvg, toggleActivePathLock, activePathLock, activePath, pageType*/ 923) {
    				each_value_3 = /*font*/ ctx[16].versions;
    				let i;

    				for (i = 0; i < each_value_3.length; i += 1) {
    					const child_ctx = get_each_context_3(ctx, each_value_3, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_3(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_3.length;
    			}
    		},
    		d(detaching) {
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach(each_1_anchor);
    		}
    	};
    }

    // (259:16) {:else}
    function create_else_block_5(ctx) {
    	let t_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].foundry + "";
    	let t;

    	return {
    		c() {
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].foundry + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (255:16) {#if data.fonts[index].url}
    function create_if_block_21(ctx) {
    	let a;
    	let t_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].foundry + "";
    	let t;
    	let a_href_value;

    	return {
    		c() {
    			a = element("a");
    			t = text(t_value);
    			attr(a, "href", a_href_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].url);
    			attr(a, "target", "_blank");
    		},
    		m(target, anchor) {
    			insert(target, a, anchor);
    			append(a, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].foundry + "")) set_data(t, t_value);

    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(a);
    		}
    	};
    }

    // (261:14) {#if data.fonts[index].vendors && data.fonts[index].vendors.length}
    function create_if_block_19(ctx) {
    	let div;
    	let t;
    	let each_value_5 = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors;
    	let each_blocks = [];

    	for (let i = 0; i < each_value_5.length; i += 1) {
    		each_blocks[i] = create_each_block_5(get_each_context_5(ctx, each_value_5, i));
    	}

    	return {
    		c() {
    			div = element("div");
    			t = text("Vendors:\n                  ");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "_padded-tb-min");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1) {
    				each_value_5 = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors;
    				let i;

    				for (i = 0; i < each_value_5.length; i += 1) {
    					const child_ctx = get_each_context_5(ctx, each_value_5, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_5(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_5.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (265:20) {#if counter != 0}
    function create_if_block_20(ctx) {
    	let t;

    	return {
    		c() {
    			t = text(",");
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (264:18) {#each data.fonts[index].vendors as vendor, counter}
    function create_each_block_5(ctx) {
    	let t0;
    	let a;
    	let t1_value = /*vendor*/ ctx[19].name + "";
    	let t1;
    	let t2;
    	let a_href_value;
    	let if_block = /*counter*/ ctx[27] != 0 && create_if_block_20();

    	return {
    		c() {
    			if (if_block) if_block.c();
    			t0 = space();
    			a = element("a");
    			t1 = text(t1_value);
    			t2 = space();
    			attr(a, "href", a_href_value = /*vendor*/ ctx[19].url);
    			attr(a, "target", "_blank");
    			attr(a, "rel", "nofollow");
    		},
    		m(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert(target, t0, anchor);
    			insert(target, a, anchor);
    			append(a, t1);
    			append(a, t2);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t1_value !== (t1_value = /*vendor*/ ctx[19].name + "")) set_data(t1, t1_value);

    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*vendor*/ ctx[19].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach(t0);
    			if (detaching) detach(a);
    		}
    	};
    }

    // (177:20) {#if version.class}
    function create_if_block_18(ctx) {
    	let t_value = /*version*/ ctx[24].class + "";
    	let t;

    	return {
    		c() {
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*version*/ ctx[24].class + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (178:20) {#if version.weight}
    function create_if_block_17(ctx) {
    	let t_value = /*version*/ ctx[24].weight + "";
    	let t;

    	return {
    		c() {
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*version*/ ctx[24].weight + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (179:20) {#if version.style}
    function create_if_block_16(ctx) {
    	let t_value = /*version*/ ctx[24].style + "";
    	let t;

    	return {
    		c() {
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*version*/ ctx[24].style + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (182:18) {#if pageType === 'feature'}
    function create_if_block_13(ctx) {
    	let svg;
    	let mounted;
    	let dispose;

    	function select_block_type_3(ctx, dirty) {
    		if (/*activePathLock*/ ctx[4] && /*activePath*/ ctx[3] === /*version*/ ctx[24].mask) return create_if_block_14;
    		if (!/*activePathLock*/ ctx[4] && /*activePath*/ ctx[3] === /*version*/ ctx[24].mask) return create_if_block_15;
    		return create_else_block_3;
    	}

    	let current_block_type = select_block_type_3(ctx);
    	let if_block = current_block_type(ctx);

    	function click_handler(...args) {
    		return /*click_handler*/ ctx[12](/*version*/ ctx[24], ...args);
    	}

    	return {
    		c() {
    			svg = svg_element("svg");
    			if_block.c();
    			attr(svg, "class", "icon -regular -eye svelte-1is5jml");
    			attr(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr(svg, "viewBox", "0 0 24 24");
    			attr(svg, "width", "24");
    			attr(svg, "height", "24");
    		},
    		m(target, anchor) {
    			insert(target, svg, anchor);
    			if_block.m(svg, null);

    			if (!mounted) {
    				dispose = listen(svg, "click", click_handler);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (current_block_type !== (current_block_type = select_block_type_3(ctx))) {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(svg, null);
    				}
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(svg);
    			if_block.d();
    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (208:22) {:else}
    function create_else_block_3(ctx) {
    	let path0;
    	let path1;

    	return {
    		c() {
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path1, "d", "M9.342 18.782l-1.931-.518.787-2.939a10.988 10.988 0\n                          0 1-3.237-1.872l-2.153 2.154-1.415-1.415\n                          2.154-2.153a10.957 10.957 0 0\n                          1-2.371-5.07l1.968-.359C3.903 10.812 7.579 14 12\n                          14c4.42 0 8.097-3.188 8.856-7.39l1.968.358a10.957\n                          10.957 0 0 1-2.37 5.071l2.153 2.153-1.415\n                          1.415-2.153-2.154a10.988 10.988 0 0 1-3.237 1.872l.787\n                          2.94-1.931.517-.788-2.94a11.072 11.072 0 0 1-3.74\n                          0l-.788 2.94z");
    		},
    		m(target, anchor) {
    			insert(target, path0, anchor);
    			insert(target, path1, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(path0);
    			if (detaching) detach(path1);
    		}
    	};
    }

    // (199:79) 
    function create_if_block_15(ctx) {
    	let path0;
    	let path1;

    	return {
    		c() {
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path1, "d", "M12 3c5.392 0 9.878 3.88 10.819 9-.94 5.12-5.427\n                          9-10.819 9-5.392 0-9.878-3.88-10.819-9C2.121 6.88\n                          6.608 3 12 3zm0 16a9.005 9.005 0 0 0 8.777-7 9.005\n                          9.005 0 0 0-17.554 0A9.005 9.005 0 0 0 12\n                          19zm0-2.5a4.5 4.5 0 1 1 0-9 4.5 4.5 0 0 1 0 9zm0-2a2.5\n                          2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z");
    		},
    		m(target, anchor) {
    			insert(target, path0, anchor);
    			insert(target, path1, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(path0);
    			if (detaching) detach(path1);
    		}
    	};
    }

    // (192:22) {#if activePathLock && activePath === version.mask}
    function create_if_block_14(ctx) {
    	let path0;
    	let path1;

    	return {
    		c() {
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path1, "d", "M1.181 12C2.121 6.88 6.608 3 12 3c5.392 0 9.878\n                          3.88 10.819 9-.94 5.12-5.427 9-10.819 9-5.392\n                          0-9.878-3.88-10.819-9zM12 17a5 5 0 1 0 0-10 5 5 0 0 0\n                          0 10zm0-2a3 3 0 1 1 0-6 3 3 0 0 1 0 6z");
    		},
    		m(target, anchor) {
    			insert(target, path0, anchor);
    			insert(target, path1, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(path0);
    			if (detaching) detach(path1);
    		}
    	};
    }

    // (231:18) {:else}
    function create_else_block_2$1(ctx) {
    	let t_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].foundry + "";
    	let t;

    	return {
    		c() {
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].foundry + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (227:18) {#if data.fonts[index].url}
    function create_if_block_12(ctx) {
    	let a;
    	let t_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].foundry + "";
    	let t;
    	let a_href_value;

    	return {
    		c() {
    			a = element("a");
    			t = text(t_value);
    			attr(a, "href", a_href_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].url);
    			attr(a, "target", "_blank");
    		},
    		m(target, anchor) {
    			insert(target, a, anchor);
    			append(a, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].foundry + "")) set_data(t, t_value);

    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(a);
    		}
    	};
    }

    // (235:16) {#if data.fonts[index].vendors && data.fonts[index].vendors.length}
    function create_if_block_10(ctx) {
    	let div;
    	let t;
    	let each_value_4 = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors;
    	let each_blocks = [];

    	for (let i = 0; i < each_value_4.length; i += 1) {
    		each_blocks[i] = create_each_block_4(get_each_context_4(ctx, each_value_4, i));
    	}

    	return {
    		c() {
    			div = element("div");
    			t = text("Vendors:\n                    ");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "_padded-tb-min");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1) {
    				each_value_4 = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors;
    				let i;

    				for (i = 0; i < each_value_4.length; i += 1) {
    					const child_ctx = get_each_context_4(ctx, each_value_4, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_4(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_4.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (239:22) {#if counter != 0}
    function create_if_block_11(ctx) {
    	let t;

    	return {
    		c() {
    			t = text(",");
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (238:20) {#each data.fonts[index].vendors as vendor, counter}
    function create_each_block_4(ctx) {
    	let t0;
    	let a;
    	let t1_value = /*vendor*/ ctx[19].name + "";
    	let t1;
    	let t2;
    	let a_href_value;
    	let if_block = /*counter*/ ctx[27] != 0 && create_if_block_11();

    	return {
    		c() {
    			if (if_block) if_block.c();
    			t0 = space();
    			a = element("a");
    			t1 = text(t1_value);
    			t2 = space();
    			attr(a, "href", a_href_value = /*vendor*/ ctx[19].url);
    			attr(a, "target", "_blank");
    			attr(a, "rel", "nofollow");
    		},
    		m(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert(target, t0, anchor);
    			insert(target, a, anchor);
    			append(a, t1);
    			append(a, t2);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t1_value !== (t1_value = /*vendor*/ ctx[19].name + "")) set_data(t1, t1_value);

    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*vendor*/ ctx[19].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach(t0);
    			if (detaching) detach(a);
    		}
    	};
    }

    // (166:12) {#each font.versions as version, i}
    function create_each_block_3(ctx) {
    	let div2;
    	let div0;
    	let h3;
    	let t0_value = /*font*/ ctx[16].name + "";
    	let t0;
    	let t1;
    	let t2;
    	let t3;
    	let t4;
    	let t5;
    	let div1;
    	let t6;
    	let t7;
    	let t8;
    	let mounted;
    	let dispose;
    	let if_block0 = /*version*/ ctx[24].class && create_if_block_18(ctx);
    	let if_block1 = /*version*/ ctx[24].weight && create_if_block_17(ctx);
    	let if_block2 = /*version*/ ctx[24].style && create_if_block_16(ctx);
    	let if_block3 = /*pageType*/ ctx[1] === "feature" && create_if_block_13(ctx);

    	function select_block_type_4(ctx, dirty) {
    		if (/*data*/ ctx[0].fonts[/*index*/ ctx[23]].url) return create_if_block_12;
    		return create_else_block_2$1;
    	}

    	let current_block_type = select_block_type_4(ctx);
    	let if_block4 = current_block_type(ctx);
    	let if_block5 = /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors && /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors.length && create_if_block_10(ctx);

    	function mouseenter_handler(...args) {
    		return /*mouseenter_handler*/ ctx[13](/*version*/ ctx[24], ...args);
    	}

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			h3 = element("h3");
    			t0 = text(t0_value);
    			t1 = space();
    			if (if_block0) if_block0.c();
    			t2 = space();
    			if (if_block1) if_block1.c();
    			t3 = space();
    			if (if_block2) if_block2.c();
    			t4 = space();
    			if (if_block3) if_block3.c();
    			t5 = space();
    			div1 = element("div");
    			t6 = text("Foundry/Designer:\n                  ");
    			if_block4.c();
    			t7 = space();
    			if (if_block5) if_block5.c();
    			t8 = space();
    			attr(div0, "class", "title svelte-1is5jml");
    			attr(div1, "class", "_padded-tb-min");
    			attr(div2, "class", "font-card _padded-min");
    		},
    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, div0);
    			append(div0, h3);
    			append(h3, t0);
    			append(h3, t1);
    			if (if_block0) if_block0.m(h3, null);
    			append(h3, t2);
    			if (if_block1) if_block1.m(h3, null);
    			append(h3, t3);
    			if (if_block2) if_block2.m(h3, null);
    			append(div0, t4);
    			if (if_block3) if_block3.m(div0, null);
    			append(div2, t5);
    			append(div2, div1);
    			append(div1, t6);
    			if_block4.m(div1, null);
    			append(div2, t7);
    			if (if_block5) if_block5.m(div2, null);
    			append(div2, t8);

    			if (!mounted) {
    				dispose = [
    					listen(div2, "mouseenter", mouseenter_handler),
    					listen(div2, "mouseleave", /*hideSvg*/ ctx[8])
    				];

    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*data*/ 1 && t0_value !== (t0_value = /*font*/ ctx[16].name + "")) set_data(t0, t0_value);

    			if (/*version*/ ctx[24].class) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_18(ctx);
    					if_block0.c();
    					if_block0.m(h3, t2);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*version*/ ctx[24].weight) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block_17(ctx);
    					if_block1.c();
    					if_block1.m(h3, t3);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (/*version*/ ctx[24].style) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_16(ctx);
    					if_block2.c();
    					if_block2.m(h3, null);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (/*pageType*/ ctx[1] === "feature") {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);
    				} else {
    					if_block3 = create_if_block_13(ctx);
    					if_block3.c();
    					if_block3.m(div0, null);
    				}
    			} else if (if_block3) {
    				if_block3.d(1);
    				if_block3 = null;
    			}

    			if (current_block_type === (current_block_type = select_block_type_4(ctx)) && if_block4) {
    				if_block4.p(ctx, dirty);
    			} else {
    				if_block4.d(1);
    				if_block4 = current_block_type(ctx);

    				if (if_block4) {
    					if_block4.c();
    					if_block4.m(div1, null);
    				}
    			}

    			if (/*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors && /*data*/ ctx[0].fonts[/*index*/ ctx[23]].vendors.length) {
    				if (if_block5) {
    					if_block5.p(ctx, dirty);
    				} else {
    					if_block5 = create_if_block_10(ctx);
    					if_block5.c();
    					if_block5.m(div2, t8);
    				}
    			} else if (if_block5) {
    				if_block5.d(1);
    				if_block5 = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div2);
    			if (if_block0) if_block0.d();
    			if (if_block1) if_block1.d();
    			if (if_block2) if_block2.d();
    			if (if_block3) if_block3.d();
    			if_block4.d();
    			if (if_block5) if_block5.d();
    			mounted = false;
    			run_all(dispose);
    		}
    	};
    }

    // (164:8) {#each data.identified as font, index}
    function create_each_block_2(ctx) {
    	let if_block_anchor;

    	function select_block_type_2(ctx, dirty) {
    		if (/*font*/ ctx[16].versions) return create_if_block_9;
    		return create_else_block_4;
    	}

    	let current_block_type = select_block_type_2(ctx);
    	let if_block = current_block_type(ctx);

    	return {
    		c() {
    			if_block.c();
    			if_block_anchor = empty();
    		},
    		m(target, anchor) {
    			if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (current_block_type === (current_block_type = select_block_type_2(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			}
    		},
    		d(detaching) {
    			if_block.d(detaching);
    			if (detaching) detach(if_block_anchor);
    		}
    	};
    }

    // (277:6) {#if data.custom && data.custom.length}
    function create_if_block_1$3(ctx) {
    	let each_1_anchor;
    	let each_value = /*data*/ ctx[0].custom;
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    	}

    	return {
    		c() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert(target, each_1_anchor, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*showSvg, data, hideSvg, toggleActivePathLock, activePathLock, activePath, pageType*/ 923) {
    				each_value = /*data*/ ctx[0].custom;
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$2(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$2(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		d(detaching) {
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach(each_1_anchor);
    		}
    	};
    }

    // (289:14) {#if pageType === 'feature'}
    function create_if_block_5$1(ctx) {
    	let svg;
    	let mounted;
    	let dispose;

    	function select_block_type_6(ctx, dirty) {
    		if (/*activePathLock*/ ctx[4] && /*activePath*/ ctx[3] === /*font*/ ctx[16].mask) return create_if_block_6$1;
    		if (!/*activePathLock*/ ctx[4] && /*activePath*/ ctx[3] === /*font*/ ctx[16].mask) return create_if_block_7$1;
    		return create_else_block_1$1;
    	}

    	let current_block_type = select_block_type_6(ctx);
    	let if_block = current_block_type(ctx);

    	function click_handler_1(...args) {
    		return /*click_handler_1*/ ctx[14](/*font*/ ctx[16], ...args);
    	}

    	return {
    		c() {
    			svg = svg_element("svg");
    			if_block.c();
    			attr(svg, "class", "icon -regular -eye svelte-1is5jml");
    			attr(svg, "xmlns", "http://www.w3.org/2000/svg");
    			attr(svg, "viewBox", "0 0 24 24");
    			attr(svg, "width", "24");
    			attr(svg, "height", "24");
    		},
    		m(target, anchor) {
    			insert(target, svg, anchor);
    			if_block.m(svg, null);

    			if (!mounted) {
    				dispose = listen(svg, "click", click_handler_1);
    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (current_block_type !== (current_block_type = select_block_type_6(ctx))) {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(svg, null);
    				}
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(svg);
    			if_block.d();
    			mounted = false;
    			dispose();
    		}
    	};
    }

    // (315:18) {:else}
    function create_else_block_1$1(ctx) {
    	let path0;
    	let path1;

    	return {
    		c() {
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path1, "d", "M9.342 18.782l-1.931-.518.787-2.939a10.988 10.988 0 0\n                      1-3.237-1.872l-2.153 2.154-1.415-1.415 2.154-2.153a10.957\n                      10.957 0 0 1-2.371-5.07l1.968-.359C3.903 10.812 7.579 14\n                      12 14c4.42 0 8.097-3.188 8.856-7.39l1.968.358a10.957\n                      10.957 0 0 1-2.37 5.071l2.153 2.153-1.415\n                      1.415-2.153-2.154a10.988 10.988 0 0 1-3.237 1.872l.787\n                      2.94-1.931.517-.788-2.94a11.072 11.072 0 0 1-3.74 0l-.788\n                      2.94z");
    		},
    		m(target, anchor) {
    			insert(target, path0, anchor);
    			insert(target, path1, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(path0);
    			if (detaching) detach(path1);
    		}
    	};
    }

    // (306:72) 
    function create_if_block_7$1(ctx) {
    	let path0;
    	let path1;

    	return {
    		c() {
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path1, "d", "M12 3c5.392 0 9.878 3.88 10.819 9-.94 5.12-5.427\n                      9-10.819 9-5.392 0-9.878-3.88-10.819-9C2.121 6.88 6.608 3\n                      12 3zm0 16a9.005 9.005 0 0 0 8.777-7 9.005 9.005 0 0\n                      0-17.554 0A9.005 9.005 0 0 0 12 19zm0-2.5a4.5 4.5 0 1 1\n                      0-9 4.5 4.5 0 0 1 0 9zm0-2a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0\n                      0 5z");
    		},
    		m(target, anchor) {
    			insert(target, path0, anchor);
    			insert(target, path1, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(path0);
    			if (detaching) detach(path1);
    		}
    	};
    }

    // (299:18) {#if activePathLock && activePath === font.mask}
    function create_if_block_6$1(ctx) {
    	let path0;
    	let path1;

    	return {
    		c() {
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			attr(path0, "fill", "none");
    			attr(path0, "d", "M0 0h24v24H0z");
    			attr(path1, "d", "M1.181 12C2.121 6.88 6.608 3 12 3c5.392 0 9.878 3.88\n                      10.819 9-.94 5.12-5.427 9-10.819 9-5.392\n                      0-9.878-3.88-10.819-9zM12 17a5 5 0 1 0 0-10 5 5 0 0 0 0\n                      10zm0-2a3 3 0 1 1 0-6 3 3 0 0 1 0 6z");
    		},
    		m(target, anchor) {
    			insert(target, path0, anchor);
    			insert(target, path1, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(path0);
    			if (detaching) detach(path1);
    		}
    	};
    }

    // (335:14) {:else}
    function create_else_block$2(ctx) {
    	let t_value = /*font*/ ctx[16].foundry + "";
    	let t;

    	return {
    		c() {
    			t = text(t_value);
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*font*/ ctx[16].foundry + "")) set_data(t, t_value);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (333:14) {#if font.url}
    function create_if_block_4$1(ctx) {
    	let a;
    	let t_value = /*font*/ ctx[16].foundry + "";
    	let t;
    	let a_href_value;

    	return {
    		c() {
    			a = element("a");
    			t = text(t_value);
    			attr(a, "href", a_href_value = /*font*/ ctx[16].url);
    			attr(a, "target", "_blank");
    		},
    		m(target, anchor) {
    			insert(target, a, anchor);
    			append(a, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*font*/ ctx[16].foundry + "")) set_data(t, t_value);

    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*font*/ ctx[16].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(a);
    		}
    	};
    }

    // (337:12) {#if font.vendors && font.vendors.length}
    function create_if_block_2$2(ctx) {
    	let div;
    	let t;
    	let each_value_1 = /*font*/ ctx[16].vendors;
    	let each_blocks = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	return {
    		c() {
    			div = element("div");
    			t = text("Vendors:\n                ");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", "_padded-tb-min");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);
    			append(div, t);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1) {
    				each_value_1 = /*font*/ ctx[16].vendors;
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block_1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value_1.length;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    // (341:18) {#if i != 0}
    function create_if_block_3$1(ctx) {
    	let t;

    	return {
    		c() {
    			t = text(",");
    		},
    		m(target, anchor) {
    			insert(target, t, anchor);
    		},
    		d(detaching) {
    			if (detaching) detach(t);
    		}
    	};
    }

    // (340:16) {#each font.vendors as vendor, i}
    function create_each_block_1(ctx) {
    	let t0;
    	let a;
    	let t1_value = /*vendor*/ ctx[19].name + "";
    	let t1;
    	let t2;
    	let a_href_value;
    	let if_block = /*i*/ ctx[21] != 0 && create_if_block_3$1();

    	return {
    		c() {
    			if (if_block) if_block.c();
    			t0 = space();
    			a = element("a");
    			t1 = text(t1_value);
    			t2 = space();
    			attr(a, "href", a_href_value = /*vendor*/ ctx[19].url);
    			attr(a, "target", "_blank");
    			attr(a, "rel", "nofollow");
    		},
    		m(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert(target, t0, anchor);
    			insert(target, a, anchor);
    			append(a, t1);
    			append(a, t2);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t1_value !== (t1_value = /*vendor*/ ctx[19].name + "")) set_data(t1, t1_value);

    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*vendor*/ ctx[19].url)) {
    				attr(a, "href", a_href_value);
    			}
    		},
    		d(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach(t0);
    			if (detaching) detach(a);
    		}
    	};
    }

    // (278:8) {#each data.custom as font}
    function create_each_block$2(ctx) {
    	let div2;
    	let div0;
    	let h3;
    	let t0_value = /*font*/ ctx[16].name + "";
    	let t0;
    	let t1;
    	let t2;
    	let div1;
    	let t3;
    	let t4;
    	let t5;
    	let mounted;
    	let dispose;
    	let if_block0 = /*pageType*/ ctx[1] === "feature" && create_if_block_5$1(ctx);

    	function select_block_type_7(ctx, dirty) {
    		if (/*font*/ ctx[16].url) return create_if_block_4$1;
    		return create_else_block$2;
    	}

    	let current_block_type = select_block_type_7(ctx);
    	let if_block1 = current_block_type(ctx);
    	let if_block2 = /*font*/ ctx[16].vendors && /*font*/ ctx[16].vendors.length && create_if_block_2$2(ctx);

    	function mouseenter_handler_1(...args) {
    		return /*mouseenter_handler_1*/ ctx[15](/*font*/ ctx[16], ...args);
    	}

    	return {
    		c() {
    			div2 = element("div");
    			div0 = element("div");
    			h3 = element("h3");
    			t0 = text(t0_value);
    			t1 = space();
    			if (if_block0) if_block0.c();
    			t2 = space();
    			div1 = element("div");
    			t3 = text("Foundry/Designer:\n              ");
    			if_block1.c();
    			t4 = space();
    			if (if_block2) if_block2.c();
    			t5 = space();
    			attr(div0, "class", "title svelte-1is5jml");
    			attr(div1, "class", "_padded-tb-min");
    			attr(div2, "class", "font-card _padded-min");
    		},
    		m(target, anchor) {
    			insert(target, div2, anchor);
    			append(div2, div0);
    			append(div0, h3);
    			append(h3, t0);
    			append(div0, t1);
    			if (if_block0) if_block0.m(div0, null);
    			append(div2, t2);
    			append(div2, div1);
    			append(div1, t3);
    			if_block1.m(div1, null);
    			append(div2, t4);
    			if (if_block2) if_block2.m(div2, null);
    			append(div2, t5);

    			if (!mounted) {
    				dispose = [
    					listen(div2, "mouseenter", mouseenter_handler_1),
    					listen(div2, "mouseleave", /*hideSvg*/ ctx[8])
    				];

    				mounted = true;
    			}
    		},
    		p(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*data*/ 1 && t0_value !== (t0_value = /*font*/ ctx[16].name + "")) set_data(t0, t0_value);

    			if (/*pageType*/ ctx[1] === "feature") {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_5$1(ctx);
    					if_block0.c();
    					if_block0.m(div0, null);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (current_block_type === (current_block_type = select_block_type_7(ctx)) && if_block1) {
    				if_block1.p(ctx, dirty);
    			} else {
    				if_block1.d(1);
    				if_block1 = current_block_type(ctx);

    				if (if_block1) {
    					if_block1.c();
    					if_block1.m(div1, null);
    				}
    			}

    			if (/*font*/ ctx[16].vendors && /*font*/ ctx[16].vendors.length) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_2$2(ctx);
    					if_block2.c();
    					if_block2.m(div2, t5);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(div2);
    			if (if_block0) if_block0.d();
    			if_block1.d();
    			if (if_block2) if_block2.d();
    			mounted = false;
    			run_all(dispose);
    		}
    	};
    }

    // (353:6) {#if pageType == 'specimen'}
    function create_if_block$3(ctx) {
    	let current;

    	const suggestiontags = new SuggestionTags({
    			props: {
    				comments: /*data*/ ctx[0].comments,
    				hasSuggestions: /*hasSuggestions*/ ctx[5]
    			}
    		});

    	return {
    		c() {
    			create_component(suggestiontags.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(suggestiontags, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const suggestiontags_changes = {};
    			if (dirty & /*data*/ 1) suggestiontags_changes.comments = /*data*/ ctx[0].comments;
    			suggestiontags.$set(suggestiontags_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(suggestiontags.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(suggestiontags.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(suggestiontags, detaching);
    		}
    	};
    }

    function create_fragment$7(ctx) {
    	let div5;
    	let div2;
    	let div1;
    	let div0;
    	let t0;
    	let img;
    	let img_srcset_value;
    	let img_src_value;
    	let img_alt_value;
    	let img_width_value;
    	let img_height_value;
    	let t1;
    	let div4;
    	let div3;
    	let h1;
    	let t2;
    	let t3;
    	let t4;
    	let t5;
    	let t6;
    	let current;
    	let if_block0 = /*pageType*/ ctx[1] === "feature" && create_if_block_27(ctx);

    	function select_block_type(ctx, dirty) {
    		if (/*data*/ ctx[0].name) return create_if_block_25;
    		return create_else_block_7;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block1 = current_block_type(ctx);
    	let if_block2 = /*pageType*/ ctx[1] == "specimen" && create_if_block_24(ctx);
    	let if_block3 = (/*data*/ ctx[0].credit && /*data*/ ctx[0].credit[0].name || /*data*/ ctx[0].credit && /*data*/ ctx[0].credit[0].url) && create_if_block_22(ctx);
    	let if_block4 = /*data*/ ctx[0].identified && /*data*/ ctx[0].identified.length && create_if_block_8(ctx);
    	let if_block5 = /*data*/ ctx[0].custom && /*data*/ ctx[0].custom.length && create_if_block_1$3(ctx);
    	let if_block6 = /*pageType*/ ctx[1] == "specimen" && create_if_block$3(ctx);

    	return {
    		c() {
    			div5 = element("div");
    			div2 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			if (if_block0) if_block0.c();
    			t0 = space();
    			img = element("img");
    			t1 = space();
    			div4 = element("div");
    			div3 = element("div");
    			h1 = element("h1");
    			if_block1.c();
    			t2 = space();
    			if (if_block2) if_block2.c();
    			t3 = space();
    			if (if_block3) if_block3.c();
    			t4 = space();
    			if (if_block4) if_block4.c();
    			t5 = space();
    			if (if_block5) if_block5.c();
    			t6 = space();
    			if (if_block6) if_block6.c();
    			attr(img, "id", "hero-img");
    			attr(img, "srcset", img_srcset_value = "" + (/*imgUrl*/ ctx[2] + "f_auto,q_70,w_600/" + /*data*/ ctx[0].image.src + " 600w, " + /*imgUrl*/ ctx[2] + "f_auto,q_70,w_1024/" + /*data*/ ctx[0].image.src + " 900w, " + /*imgUrl*/ ctx[2] + "f_auto,q_70,w_1440/" + /*data*/ ctx[0].image.src + " 1440w"));
    			if (img.src !== (img_src_value = "" + (/*imgUrl*/ ctx[2] + "f_auto,q_70,w_600/" + /*data*/ ctx[0].image.src))) attr(img, "src", img_src_value);
    			attr(img, "alt", img_alt_value = /*data*/ ctx[0].name);
    			attr(img, "width", img_width_value = /*data*/ ctx[0].image.width);
    			attr(img, "height", img_height_value = /*data*/ ctx[0].image.height);
    			attr(div0, "id", "hero-wrapper");
    			attr(div1, "class", "pane _padded svelte-1is5jml");
    			attr(div2, "class", "showcase-pane image svelte-1is5jml");
    			attr(div2, "id", "average-color");
    			set_style(div2, "background-color", /*average_colour*/ ctx[6]);
    			attr(h1, "class", "title svelte-1is5jml");
    			attr(div3, "class", "pane _padded svelte-1is5jml");
    			attr(div4, "class", "showcase-pane meta svelte-1is5jml");
    			attr(div5, "class", "ui-showcase svelte-1is5jml");
    		},
    		m(target, anchor) {
    			insert(target, div5, anchor);
    			append(div5, div2);
    			append(div2, div1);
    			append(div1, div0);
    			if (if_block0) if_block0.m(div0, null);
    			append(div0, t0);
    			append(div0, img);
    			append(div5, t1);
    			append(div5, div4);
    			append(div4, div3);
    			append(div3, h1);
    			if_block1.m(h1, null);
    			append(div3, t2);
    			if (if_block2) if_block2.m(div3, null);
    			append(div3, t3);
    			if (if_block3) if_block3.m(div3, null);
    			append(div3, t4);
    			if (if_block4) if_block4.m(div3, null);
    			append(div3, t5);
    			if (if_block5) if_block5.m(div3, null);
    			append(div3, t6);
    			if (if_block6) if_block6.m(div3, null);
    			current = true;
    		},
    		p(ctx, [dirty]) {
    			if (/*pageType*/ ctx[1] === "feature") {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_27(ctx);
    					if_block0.c();
    					if_block0.m(div0, t0);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (!current || dirty & /*imgUrl, data*/ 5 && img_srcset_value !== (img_srcset_value = "" + (/*imgUrl*/ ctx[2] + "f_auto,q_70,w_600/" + /*data*/ ctx[0].image.src + " 600w, " + /*imgUrl*/ ctx[2] + "f_auto,q_70,w_1024/" + /*data*/ ctx[0].image.src + " 900w, " + /*imgUrl*/ ctx[2] + "f_auto,q_70,w_1440/" + /*data*/ ctx[0].image.src + " 1440w"))) {
    				attr(img, "srcset", img_srcset_value);
    			}

    			if (!current || dirty & /*imgUrl, data*/ 5 && img.src !== (img_src_value = "" + (/*imgUrl*/ ctx[2] + "f_auto,q_70,w_600/" + /*data*/ ctx[0].image.src))) {
    				attr(img, "src", img_src_value);
    			}

    			if (!current || dirty & /*data*/ 1 && img_alt_value !== (img_alt_value = /*data*/ ctx[0].name)) {
    				attr(img, "alt", img_alt_value);
    			}

    			if (!current || dirty & /*data*/ 1 && img_width_value !== (img_width_value = /*data*/ ctx[0].image.width)) {
    				attr(img, "width", img_width_value);
    			}

    			if (!current || dirty & /*data*/ 1 && img_height_value !== (img_height_value = /*data*/ ctx[0].image.height)) {
    				attr(img, "height", img_height_value);
    			}

    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block1) {
    				if_block1.p(ctx, dirty);
    			} else {
    				if_block1.d(1);
    				if_block1 = current_block_type(ctx);

    				if (if_block1) {
    					if_block1.c();
    					if_block1.m(h1, null);
    				}
    			}

    			if (/*pageType*/ ctx[1] == "specimen") {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block_24(ctx);
    					if_block2.c();
    					if_block2.m(div3, t3);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (/*data*/ ctx[0].credit && /*data*/ ctx[0].credit[0].name || /*data*/ ctx[0].credit && /*data*/ ctx[0].credit[0].url) {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);
    				} else {
    					if_block3 = create_if_block_22(ctx);
    					if_block3.c();
    					if_block3.m(div3, t4);
    				}
    			} else if (if_block3) {
    				if_block3.d(1);
    				if_block3 = null;
    			}

    			if (/*data*/ ctx[0].identified && /*data*/ ctx[0].identified.length) {
    				if (if_block4) {
    					if_block4.p(ctx, dirty);
    				} else {
    					if_block4 = create_if_block_8(ctx);
    					if_block4.c();
    					if_block4.m(div3, t5);
    				}
    			} else if (if_block4) {
    				if_block4.d(1);
    				if_block4 = null;
    			}

    			if (/*data*/ ctx[0].custom && /*data*/ ctx[0].custom.length) {
    				if (if_block5) {
    					if_block5.p(ctx, dirty);
    				} else {
    					if_block5 = create_if_block_1$3(ctx);
    					if_block5.c();
    					if_block5.m(div3, t6);
    				}
    			} else if (if_block5) {
    				if_block5.d(1);
    				if_block5 = null;
    			}

    			if (/*pageType*/ ctx[1] == "specimen") {
    				if (if_block6) {
    					if_block6.p(ctx, dirty);

    					if (dirty & /*pageType*/ 2) {
    						transition_in(if_block6, 1);
    					}
    				} else {
    					if_block6 = create_if_block$3(ctx);
    					if_block6.c();
    					transition_in(if_block6, 1);
    					if_block6.m(div3, null);
    				}
    			} else if (if_block6) {
    				group_outros();

    				transition_out(if_block6, 1, 1, () => {
    					if_block6 = null;
    				});

    				check_outros();
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(if_block6);
    			current = true;
    		},
    		o(local) {
    			transition_out(if_block6);
    			current = false;
    		},
    		d(detaching) {
    			if (detaching) detach(div5);
    			if (if_block0) if_block0.d();
    			if_block1.d();
    			if (if_block2) if_block2.d();
    			if (if_block3) if_block3.d();
    			if (if_block4) if_block4.d();
    			if (if_block5) if_block5.d();
    			if (if_block6) if_block6.d();
    		}
    	};
    }

    function instance$7($$self, $$props, $$invalidate) {
    	let { data } = $$props,
    		{ pageType = page } = $$props,
    		{ imgUrl = "https://res.cloudinary.com/typereel/image/upload/" } = $$props;

    	let activePath, activePathLock;

    	let suggestions = data.comments.filter(comment => {
    		return comment.type != "comment" && comment.type != "text" && comment.type != "font";
    	});

    	let hasSuggestions = suggestions.length ? true : false;

    	let average_colour = data.image.average_colour
    	? data.image.average_colour
    	: "#222";

    	function showSvg(path) {
    		if (activePathLock) return;
    		$$invalidate(3, activePath = path);
    	}

    	function hideSvg() {
    		if (activePathLock) return;
    		$$invalidate(3, activePath = null);
    	}

    	function toggleActivePathLock(path) {
    		if (activePathLock && path === activePath) {
    			$$invalidate(3, activePath = null);
    			$$invalidate(4, activePathLock = false);
    		} else {
    			$$invalidate(3, activePath = path);
    			$$invalidate(4, activePathLock = true);
    		}
    	}

    	function removeActivePathLock() {
    		$$invalidate(3, activePath = null);
    		$$invalidate(4, activePathLock = false);
    	}

    	onMount(async () => {
    		let specimen = document.getElementById("hero-img");
    		let specimenMask = document.getElementById("hero-mask");
    		let specimenMaskTrigger = document.getElementById("mask-trigger-0");
    		let specimenMaskTrigger2 = document.getElementById("mask-trigger-1");
    		let specimenMaskPath = document.getElementById("mask-path");
    	});

    	const click_handler = version => {
    		toggleActivePathLock(version.mask);
    	};

    	const mouseenter_handler = version => {
    		showSvg(version.mask);
    	};

    	const click_handler_1 = font => {
    		toggleActivePathLock(font.mask);
    	};

    	const mouseenter_handler_1 = font => {
    		showSvg(font.mask);
    	};

    	$$self.$set = $$props => {
    		if ("data" in $$props) $$invalidate(0, data = $$props.data);
    		if ("pageType" in $$props) $$invalidate(1, pageType = $$props.pageType);
    		if ("imgUrl" in $$props) $$invalidate(2, imgUrl = $$props.imgUrl);
    	};

    	return [
    		data,
    		pageType,
    		imgUrl,
    		activePath,
    		activePathLock,
    		hasSuggestions,
    		average_colour,
    		showSvg,
    		hideSvg,
    		toggleActivePathLock,
    		suggestions,
    		removeActivePathLock,
    		click_handler,
    		mouseenter_handler,
    		click_handler_1,
    		mouseenter_handler_1
    	];
    }

    class Showcase extends SvelteComponent {
    	constructor(options) {
    		super();
    		if (!document_1$2.getElementById("svelte-1is5jml-style")) add_css$3();
    		init(this, options, instance$7, create_fragment$7, safe_not_equal, { data: 0, pageType: 1, imgUrl: 2 });
    	}
    }

    /* src/components/client/Lightbox.svelte generated by Svelte v3.23.0 */

    function create_if_block$4(ctx) {
    	let div1;
    	let div0;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			attr(div0, "class", "panel");
    			attr(div1, "class", "ui-lightbox");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			div0.innerHTML = /*lightboxContent*/ ctx[1];

    			if (!mounted) {
    				dispose = listen(div1, "click", /*toggleLightbox*/ ctx[2]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*lightboxContent*/ 2) div0.innerHTML = /*lightboxContent*/ ctx[1];		},
    		d(detaching) {
    			if (detaching) detach(div1);
    			mounted = false;
    			dispose();
    		}
    	};
    }

    function create_fragment$8(ctx) {
    	let if_block_anchor;
    	let if_block = /*lightboxIsActive*/ ctx[0] && create_if_block$4(ctx);

    	return {
    		c() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(ctx, [dirty]) {
    			if (/*lightboxIsActive*/ ctx[0]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block$4(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach(if_block_anchor);
    		}
    	};
    }

    function instance$8($$self, $$props, $$invalidate) {
    	const dispatch = createEventDispatcher();
    	let { lightboxIsActive = false } = $$props, { lightboxContent } = $$props;

    	function toggleLightbox() {
    		dispatch("fireLightbox");
    	}

    	$$self.$set = $$props => {
    		if ("lightboxIsActive" in $$props) $$invalidate(0, lightboxIsActive = $$props.lightboxIsActive);
    		if ("lightboxContent" in $$props) $$invalidate(1, lightboxContent = $$props.lightboxContent);
    	};

    	return [lightboxIsActive, lightboxContent, toggleLightbox];
    }

    class Lightbox extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$8, create_fragment$8, safe_not_equal, { lightboxIsActive: 0, lightboxContent: 1 });
    	}
    }

    /* src/components/client/AdminBar.svelte generated by Svelte v3.23.0 */

    function create_if_block$5(ctx) {
    	let div1;
    	let div0;
    	let button;
    	let t0;
    	let t1;
    	let t2;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			div1 = element("div");
    			div0 = element("div");
    			button = element("button");
    			t0 = text("Move ");
    			t1 = text(/*specimenId*/ ctx[1]);
    			t2 = text(" to inspiration reel");
    			attr(button, "class", "ui-button");
    			attr(div0, "class", "ui-container");
    			attr(div1, "class", "admin-bar");
    		},
    		m(target, anchor) {
    			insert(target, div1, anchor);
    			append(div1, div0);
    			append(div0, button);
    			append(button, t0);
    			append(button, t1);
    			append(button, t2);

    			if (!mounted) {
    				dispose = listen(button, "click", /*moveSpecimenToInspirationReel*/ ctx[2]);
    				mounted = true;
    			}
    		},
    		p(ctx, dirty) {
    			if (dirty & /*specimenId*/ 2) set_data(t1, /*specimenId*/ ctx[1]);
    		},
    		d(detaching) {
    			if (detaching) detach(div1);
    			mounted = false;
    			dispose();
    		}
    	};
    }

    function create_fragment$9(ctx) {
    	let if_block_anchor;
    	let if_block = /*role*/ ctx[0] && /*role*/ ctx[0] == "admin" && create_if_block$5(ctx);

    	return {
    		c() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert(target, if_block_anchor, anchor);
    		},
    		p(ctx, [dirty]) {
    			if (/*role*/ ctx[0] && /*role*/ ctx[0] == "admin") {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block$5(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach(if_block_anchor);
    		}
    	};
    }

    function instance$9($$self, $$props, $$invalidate) {
    	let { role } = $$props, { specimenId } = $$props;

    	function moveSpecimenToInspirationReel() {
    		fetch("/specimen/move", {
    			headers: {
    				Accept: "application/json",
    				"Content-Type": "application/json"
    			},
    			method: "POST",
    			body: JSON.stringify({ specimenId })
    		}).then(
    			res => {
    				console.log(res);
    			},
    			function () {
    				console.log("something went wrong");
    			}
    		);
    	}

    	$$self.$set = $$props => {
    		if ("role" in $$props) $$invalidate(0, role = $$props.role);
    		if ("specimenId" in $$props) $$invalidate(1, specimenId = $$props.specimenId);
    	};

    	return [role, specimenId, moveSpecimenToInspirationReel];
    }

    class AdminBar extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$9, create_fragment$9, safe_not_equal, { role: 0, specimenId: 1 });
    	}
    }

    /* src/components/client/MainNav.svelte generated by Svelte v3.23.0 */

    const { document: document_1$3 } = globals;

    function add_css$4() {
    	var style = element("style");
    	style.id = "svelte-tkzrph-style";
    	style.textContent = ".logo.svelte-tkzrph a.svelte-tkzrph,.logo.svelte-tkzrph img.svelte-tkzrph{display:block}";
    	append(document_1$3.head, style);
    }

    function create_fragment$a(ctx) {
    	let div4;
    	let div3;
    	let div2;
    	let div0;
    	let t0;
    	let div1;
    	let button;
    	let t1;
    	let ul;
    	let li0;
    	let t3;
    	let li1;
    	let t5;
    	let li2;
    	let a3;
    	let t6;
    	let a3_href_value;
    	let t7;
    	let li3;
    	let a4;
    	let t8;
    	let a4_href_value;
    	let mounted;
    	let dispose;

    	return {
    		c() {
    			div4 = element("div");
    			div3 = element("div");
    			div2 = element("div");
    			div0 = element("div");
    			div0.innerHTML = `<a href="/" class="svelte-tkzrph"><img src="/images/logo-header.svg" width="200" height="49" class="svelte-tkzrph"></a>`;
    			t0 = space();
    			div1 = element("div");
    			button = element("button");
    			button.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"></path><path d="M3 4h18v2H3V4zm0 7h18v2H3v-2zm0 7h18v2H3v-2z"></path></svg>`;
    			t1 = space();
    			ul = element("ul");
    			li0 = element("li");
    			li0.innerHTML = `<a href="/submission">Upload Specimen</a>`;
    			t3 = space();
    			li1 = element("li");
    			li1.innerHTML = `<a href="/about">About Typereel</a>`;
    			t5 = space();
    			li2 = element("li");
    			a3 = element("a");
    			t6 = text("Login");
    			t7 = space();
    			li3 = element("li");
    			a4 = element("a");
    			t8 = text("Register");
    			attr(div0, "class", "logo svelte-tkzrph");
    			attr(div1, "class", "icon");
    			attr(div2, "class", "bar");

    			attr(a3, "href", a3_href_value = "/login" + (/*returnToUrl*/ ctx[0]
    			? "?referrer=" + /*returnToUrl*/ ctx[0]
    			: ""));

    			attr(a4, "href", a4_href_value = "/register" + (/*returnToUrl*/ ctx[0]
    			? "?referrer=" + /*returnToUrl*/ ctx[0]
    			: ""));

    			attr(a4, "class", "ui-button -small");
    			attr(ul, "class", "ui-nav -align-end -dark -hidden");
    			attr(ul, "id", "main-nav");
    			attr(div3, "class", "ui-header container");
    			attr(div4, "class", "_padded-t-med");
    		},
    		m(target, anchor) {
    			insert(target, div4, anchor);
    			append(div4, div3);
    			append(div3, div2);
    			append(div2, div0);
    			append(div2, t0);
    			append(div2, div1);
    			append(div1, button);
    			append(div3, t1);
    			append(div3, ul);
    			append(ul, li0);
    			append(ul, t3);
    			append(ul, li1);
    			append(ul, t5);
    			append(ul, li2);
    			append(li2, a3);
    			append(a3, t6);
    			append(ul, t7);
    			append(ul, li3);
    			append(li3, a4);
    			append(a4, t8);

    			if (!mounted) {
    				dispose = listen(button, "click", toggleMenu);
    				mounted = true;
    			}
    		},
    		p(ctx, [dirty]) {
    			if (dirty & /*returnToUrl*/ 1 && a3_href_value !== (a3_href_value = "/login" + (/*returnToUrl*/ ctx[0]
    			? "?referrer=" + /*returnToUrl*/ ctx[0]
    			: ""))) {
    				attr(a3, "href", a3_href_value);
    			}

    			if (dirty & /*returnToUrl*/ 1 && a4_href_value !== (a4_href_value = "/register" + (/*returnToUrl*/ ctx[0]
    			? "?referrer=" + /*returnToUrl*/ ctx[0]
    			: ""))) {
    				attr(a4, "href", a4_href_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(div4);
    			mounted = false;
    			dispose();
    		}
    	};
    }

    function toggleMenu() {
    	const nav = document.querySelector("#main-nav");
    	nav.classList.toggle("-hidden");
    }

    function instance$a($$self, $$props, $$invalidate) {
    	let { returnToUrl } = $$props;

    	$$self.$set = $$props => {
    		if ("returnToUrl" in $$props) $$invalidate(0, returnToUrl = $$props.returnToUrl);
    	};

    	return [returnToUrl];
    }

    class MainNav extends SvelteComponent {
    	constructor(options) {
    		super();
    		if (!document_1$3.getElementById("svelte-tkzrph-style")) add_css$4();
    		init(this, options, instance$a, create_fragment$a, safe_not_equal, { returnToUrl: 0 });
    	}
    }

    /* src/components/client/DuoSwitch.svelte generated by Svelte v3.23.0 */

    function get_each_context$3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[1] = list[i];
    	return child_ctx;
    }

    // (2:2) {#each data.links as link}
    function create_each_block$3(ctx) {
    	let a;
    	let t_value = /*link*/ ctx[1].label + "";
    	let t;
    	let a_href_value;
    	let a_class_value;

    	return {
    		c() {
    			a = element("a");
    			t = text(t_value);
    			attr(a, "href", a_href_value = /*link*/ ctx[1].url);
    			attr(a, "class", a_class_value = "ui-button -outline " + (/*link*/ ctx[1].active ? "-active" : ""));
    		},
    		m(target, anchor) {
    			insert(target, a, anchor);
    			append(a, t);
    		},
    		p(ctx, dirty) {
    			if (dirty & /*data*/ 1 && t_value !== (t_value = /*link*/ ctx[1].label + "")) set_data(t, t_value);

    			if (dirty & /*data*/ 1 && a_href_value !== (a_href_value = /*link*/ ctx[1].url)) {
    				attr(a, "href", a_href_value);
    			}

    			if (dirty & /*data*/ 1 && a_class_value !== (a_class_value = "ui-button -outline " + (/*link*/ ctx[1].active ? "-active" : ""))) {
    				attr(a, "class", a_class_value);
    			}
    		},
    		d(detaching) {
    			if (detaching) detach(a);
    		}
    	};
    }

    function create_fragment$b(ctx) {
    	let div;
    	let div_class_value;
    	let each_value = /*data*/ ctx[0].links;
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$3(get_each_context$3(ctx, each_value, i));
    	}

    	return {
    		c() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr(div, "class", div_class_value = "ui-tabs " + (/*data*/ ctx[0].feature ? "-feature" : "") + " _spaced-t-med");
    		},
    		m(target, anchor) {
    			insert(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p(ctx, [dirty]) {
    			if (dirty & /*data*/ 1) {
    				each_value = /*data*/ ctx[0].links;
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$3(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$3(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (dirty & /*data*/ 1 && div_class_value !== (div_class_value = "ui-tabs " + (/*data*/ ctx[0].feature ? "-feature" : "") + " _spaced-t-med")) {
    				attr(div, "class", div_class_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d(detaching) {
    			if (detaching) detach(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};
    }

    function instance$b($$self, $$props, $$invalidate) {
    	let { data } = $$props;

    	$$self.$set = $$props => {
    		if ("data" in $$props) $$invalidate(0, data = $$props.data);
    	};

    	return [data];
    }

    class DuoSwitch extends SvelteComponent {
    	constructor(options) {
    		super();
    		init(this, options, instance$b, create_fragment$b, safe_not_equal, { data: 0 });
    	}
    }

    /* src/components/client/App.svelte generated by Svelte v3.23.0 */

    function create_if_block_5$2(ctx) {
    	let current;

    	const mainnav = new MainNav({
    			props: { returnToUrl: /*returnToUrl*/ ctx[3] }
    		});

    	return {
    		c() {
    			create_component(mainnav.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(mainnav, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const mainnav_changes = {};
    			if (dirty & /*returnToUrl*/ 8) mainnav_changes.returnToUrl = /*returnToUrl*/ ctx[3];
    			mainnav.$set(mainnav_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(mainnav.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(mainnav.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(mainnav, detaching);
    		}
    	};
    }

    // (47:0) {#if page == 'reel'}
    function create_if_block_4$2(ctx) {
    	let t0;
    	let div;
    	let t1;
    	let current;

    	const mainnav = new MainNav({
    			props: { returnToUrl: /*returnToUrl*/ ctx[3] }
    		});

    	const duoswitch = new DuoSwitch({
    			props: { data: /*duoswitchData*/ ctx[7] }
    		});

    	const reel = new Reel({
    			props: {
    				reelData: /*staticData*/ ctx[4],
    				page: /*page*/ ctx[2]
    			}
    		});

    	return {
    		c() {
    			create_component(mainnav.$$.fragment);
    			t0 = space();
    			div = element("div");
    			create_component(duoswitch.$$.fragment);
    			t1 = space();
    			create_component(reel.$$.fragment);
    			attr(div, "class", "ui-container -narrow _spaced-med");
    		},
    		m(target, anchor) {
    			mount_component(mainnav, target, anchor);
    			insert(target, t0, anchor);
    			insert(target, div, anchor);
    			mount_component(duoswitch, div, null);
    			insert(target, t1, anchor);
    			mount_component(reel, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const mainnav_changes = {};
    			if (dirty & /*returnToUrl*/ 8) mainnav_changes.returnToUrl = /*returnToUrl*/ ctx[3];
    			mainnav.$set(mainnav_changes);
    			const reel_changes = {};
    			if (dirty & /*staticData*/ 16) reel_changes.reelData = /*staticData*/ ctx[4];
    			if (dirty & /*page*/ 4) reel_changes.page = /*page*/ ctx[2];
    			reel.$set(reel_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(mainnav.$$.fragment, local);
    			transition_in(duoswitch.$$.fragment, local);
    			transition_in(reel.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(mainnav.$$.fragment, local);
    			transition_out(duoswitch.$$.fragment, local);
    			transition_out(reel.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(mainnav, detaching);
    			if (detaching) detach(t0);
    			if (detaching) detach(div);
    			destroy_component(duoswitch);
    			if (detaching) detach(t1);
    			destroy_component(reel, detaching);
    		}
    	};
    }

    // (55:0) {#if page == 'inspiration'}
    function create_if_block_3$2(ctx) {
    	let t0;
    	let div;
    	let t1;
    	let current;

    	const mainnav = new MainNav({
    			props: { returnToUrl: /*returnToUrl*/ ctx[3] }
    		});

    	const duoswitch = new DuoSwitch({
    			props: { data: /*duoswitchData*/ ctx[7] }
    		});

    	const reel = new Reel({
    			props: {
    				reelData: /*staticData*/ ctx[4],
    				page: /*page*/ ctx[2]
    			}
    		});

    	return {
    		c() {
    			create_component(mainnav.$$.fragment);
    			t0 = space();
    			div = element("div");
    			create_component(duoswitch.$$.fragment);
    			t1 = space();
    			create_component(reel.$$.fragment);
    			attr(div, "class", "ui-container -narrow _spaced-med");
    		},
    		m(target, anchor) {
    			mount_component(mainnav, target, anchor);
    			insert(target, t0, anchor);
    			insert(target, div, anchor);
    			mount_component(duoswitch, div, null);
    			insert(target, t1, anchor);
    			mount_component(reel, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const mainnav_changes = {};
    			if (dirty & /*returnToUrl*/ 8) mainnav_changes.returnToUrl = /*returnToUrl*/ ctx[3];
    			mainnav.$set(mainnav_changes);
    			const reel_changes = {};
    			if (dirty & /*staticData*/ 16) reel_changes.reelData = /*staticData*/ ctx[4];
    			if (dirty & /*page*/ 4) reel_changes.page = /*page*/ ctx[2];
    			reel.$set(reel_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(mainnav.$$.fragment, local);
    			transition_in(duoswitch.$$.fragment, local);
    			transition_in(reel.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(mainnav.$$.fragment, local);
    			transition_out(duoswitch.$$.fragment, local);
    			transition_out(reel.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(mainnav, detaching);
    			if (detaching) detach(t0);
    			if (detaching) detach(div);
    			destroy_component(duoswitch);
    			if (detaching) detach(t1);
    			destroy_component(reel, detaching);
    		}
    	};
    }

    // (63:0) {#if page == 'specimen' || page == 'feature'}
    function create_if_block_2$3(ctx) {
    	let t0;
    	let t1;
    	let t2;
    	let current;

    	const mainnav = new MainNav({
    			props: { returnToUrl: /*returnToUrl*/ ctx[3] }
    		});

    	const showcase = new Showcase({ props: { data: /*staticData*/ ctx[4] } });

    	const commentthread = new CommentThread({
    			props: {
    				commentData: /*staticData*/ ctx[4],
    				returnToUrl: /*returnToUrl*/ ctx[3]
    			}
    		});

    	commentthread.$on("fireLightbox", /*toggleLightbox*/ ctx[6]);

    	const lightbox = new Lightbox({
    			props: {
    				lightboxIsActive: /*lightboxIsActive*/ ctx[0],
    				lightboxContent: /*lightboxContent*/ ctx[1]
    			}
    		});

    	lightbox.$on("fireLightbox", /*toggleLightbox*/ ctx[6]);

    	return {
    		c() {
    			create_component(mainnav.$$.fragment);
    			t0 = space();
    			create_component(showcase.$$.fragment);
    			t1 = space();
    			create_component(commentthread.$$.fragment);
    			t2 = space();
    			create_component(lightbox.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(mainnav, target, anchor);
    			insert(target, t0, anchor);
    			mount_component(showcase, target, anchor);
    			insert(target, t1, anchor);
    			mount_component(commentthread, target, anchor);
    			insert(target, t2, anchor);
    			mount_component(lightbox, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const mainnav_changes = {};
    			if (dirty & /*returnToUrl*/ 8) mainnav_changes.returnToUrl = /*returnToUrl*/ ctx[3];
    			mainnav.$set(mainnav_changes);
    			const showcase_changes = {};
    			if (dirty & /*staticData*/ 16) showcase_changes.data = /*staticData*/ ctx[4];
    			showcase.$set(showcase_changes);
    			const commentthread_changes = {};
    			if (dirty & /*staticData*/ 16) commentthread_changes.commentData = /*staticData*/ ctx[4];
    			if (dirty & /*returnToUrl*/ 8) commentthread_changes.returnToUrl = /*returnToUrl*/ ctx[3];
    			commentthread.$set(commentthread_changes);
    			const lightbox_changes = {};
    			if (dirty & /*lightboxIsActive*/ 1) lightbox_changes.lightboxIsActive = /*lightboxIsActive*/ ctx[0];
    			if (dirty & /*lightboxContent*/ 2) lightbox_changes.lightboxContent = /*lightboxContent*/ ctx[1];
    			lightbox.$set(lightbox_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(mainnav.$$.fragment, local);
    			transition_in(showcase.$$.fragment, local);
    			transition_in(commentthread.$$.fragment, local);
    			transition_in(lightbox.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(mainnav.$$.fragment, local);
    			transition_out(showcase.$$.fragment, local);
    			transition_out(commentthread.$$.fragment, local);
    			transition_out(lightbox.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(mainnav, detaching);
    			if (detaching) detach(t0);
    			destroy_component(showcase, detaching);
    			if (detaching) detach(t1);
    			destroy_component(commentthread, detaching);
    			if (detaching) detach(t2);
    			destroy_component(lightbox, detaching);
    		}
    	};
    }

    // (79:0) {#if page == 'submission'}
    function create_if_block_1$4(ctx) {
    	let current;
    	const submissionform = new SubmissionForm({});

    	return {
    		c() {
    			create_component(submissionform.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(submissionform, target, anchor);
    			current = true;
    		},
    		i(local) {
    			if (current) return;
    			transition_in(submissionform.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(submissionform.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(submissionform, detaching);
    		}
    	};
    }

    // (83:0) {#if page == 'specimen'}
    function create_if_block$6(ctx) {
    	let current;

    	const adminbar = new AdminBar({
    			props: {
    				role: /*role*/ ctx[5],
    				specimenId: /*staticData*/ ctx[4]._id
    			}
    		});

    	return {
    		c() {
    			create_component(adminbar.$$.fragment);
    		},
    		m(target, anchor) {
    			mount_component(adminbar, target, anchor);
    			current = true;
    		},
    		p(ctx, dirty) {
    			const adminbar_changes = {};
    			if (dirty & /*role*/ 32) adminbar_changes.role = /*role*/ ctx[5];
    			if (dirty & /*staticData*/ 16) adminbar_changes.specimenId = /*staticData*/ ctx[4]._id;
    			adminbar.$set(adminbar_changes);
    		},
    		i(local) {
    			if (current) return;
    			transition_in(adminbar.$$.fragment, local);
    			current = true;
    		},
    		o(local) {
    			transition_out(adminbar.$$.fragment, local);
    			current = false;
    		},
    		d(detaching) {
    			destroy_component(adminbar, detaching);
    		}
    	};
    }

    function create_fragment$c(ctx) {
    	let t0;
    	let t1;
    	let t2;
    	let t3;
    	let t4;
    	let if_block5_anchor;
    	let current;
    	let if_block0 = /*page*/ ctx[2] == "home" && create_if_block_5$2(ctx);
    	let if_block1 = /*page*/ ctx[2] == "reel" && create_if_block_4$2(ctx);
    	let if_block2 = /*page*/ ctx[2] == "inspiration" && create_if_block_3$2(ctx);
    	let if_block3 = (/*page*/ ctx[2] == "specimen" || /*page*/ ctx[2] == "feature") && create_if_block_2$3(ctx);
    	let if_block4 = /*page*/ ctx[2] == "submission" && create_if_block_1$4();
    	let if_block5 = /*page*/ ctx[2] == "specimen" && create_if_block$6(ctx);

    	return {
    		c() {
    			if (if_block0) if_block0.c();
    			t0 = space();
    			if (if_block1) if_block1.c();
    			t1 = space();
    			if (if_block2) if_block2.c();
    			t2 = space();
    			if (if_block3) if_block3.c();
    			t3 = space();
    			if (if_block4) if_block4.c();
    			t4 = space();
    			if (if_block5) if_block5.c();
    			if_block5_anchor = empty();
    		},
    		m(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert(target, t0, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert(target, t1, anchor);
    			if (if_block2) if_block2.m(target, anchor);
    			insert(target, t2, anchor);
    			if (if_block3) if_block3.m(target, anchor);
    			insert(target, t3, anchor);
    			if (if_block4) if_block4.m(target, anchor);
    			insert(target, t4, anchor);
    			if (if_block5) if_block5.m(target, anchor);
    			insert(target, if_block5_anchor, anchor);
    			current = true;
    		},
    		p(ctx, [dirty]) {
    			if (/*page*/ ctx[2] == "home") {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);

    					if (dirty & /*page*/ 4) {
    						transition_in(if_block0, 1);
    					}
    				} else {
    					if_block0 = create_if_block_5$2(ctx);
    					if_block0.c();
    					transition_in(if_block0, 1);
    					if_block0.m(t0.parentNode, t0);
    				}
    			} else if (if_block0) {
    				group_outros();

    				transition_out(if_block0, 1, 1, () => {
    					if_block0 = null;
    				});

    				check_outros();
    			}

    			if (/*page*/ ctx[2] == "reel") {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);

    					if (dirty & /*page*/ 4) {
    						transition_in(if_block1, 1);
    					}
    				} else {
    					if_block1 = create_if_block_4$2(ctx);
    					if_block1.c();
    					transition_in(if_block1, 1);
    					if_block1.m(t1.parentNode, t1);
    				}
    			} else if (if_block1) {
    				group_outros();

    				transition_out(if_block1, 1, 1, () => {
    					if_block1 = null;
    				});

    				check_outros();
    			}

    			if (/*page*/ ctx[2] == "inspiration") {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);

    					if (dirty & /*page*/ 4) {
    						transition_in(if_block2, 1);
    					}
    				} else {
    					if_block2 = create_if_block_3$2(ctx);
    					if_block2.c();
    					transition_in(if_block2, 1);
    					if_block2.m(t2.parentNode, t2);
    				}
    			} else if (if_block2) {
    				group_outros();

    				transition_out(if_block2, 1, 1, () => {
    					if_block2 = null;
    				});

    				check_outros();
    			}

    			if (/*page*/ ctx[2] == "specimen" || /*page*/ ctx[2] == "feature") {
    				if (if_block3) {
    					if_block3.p(ctx, dirty);

    					if (dirty & /*page*/ 4) {
    						transition_in(if_block3, 1);
    					}
    				} else {
    					if_block3 = create_if_block_2$3(ctx);
    					if_block3.c();
    					transition_in(if_block3, 1);
    					if_block3.m(t3.parentNode, t3);
    				}
    			} else if (if_block3) {
    				group_outros();

    				transition_out(if_block3, 1, 1, () => {
    					if_block3 = null;
    				});

    				check_outros();
    			}

    			if (/*page*/ ctx[2] == "submission") {
    				if (if_block4) {
    					if (dirty & /*page*/ 4) {
    						transition_in(if_block4, 1);
    					}
    				} else {
    					if_block4 = create_if_block_1$4();
    					if_block4.c();
    					transition_in(if_block4, 1);
    					if_block4.m(t4.parentNode, t4);
    				}
    			} else if (if_block4) {
    				group_outros();

    				transition_out(if_block4, 1, 1, () => {
    					if_block4 = null;
    				});

    				check_outros();
    			}

    			if (/*page*/ ctx[2] == "specimen") {
    				if (if_block5) {
    					if_block5.p(ctx, dirty);

    					if (dirty & /*page*/ 4) {
    						transition_in(if_block5, 1);
    					}
    				} else {
    					if_block5 = create_if_block$6(ctx);
    					if_block5.c();
    					transition_in(if_block5, 1);
    					if_block5.m(if_block5_anchor.parentNode, if_block5_anchor);
    				}
    			} else if (if_block5) {
    				group_outros();

    				transition_out(if_block5, 1, 1, () => {
    					if_block5 = null;
    				});

    				check_outros();
    			}
    		},
    		i(local) {
    			if (current) return;
    			transition_in(if_block0);
    			transition_in(if_block1);
    			transition_in(if_block2);
    			transition_in(if_block3);
    			transition_in(if_block4);
    			transition_in(if_block5);
    			current = true;
    		},
    		o(local) {
    			transition_out(if_block0);
    			transition_out(if_block1);
    			transition_out(if_block2);
    			transition_out(if_block3);
    			transition_out(if_block4);
    			transition_out(if_block5);
    			current = false;
    		},
    		d(detaching) {
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach(t0);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach(t1);
    			if (if_block2) if_block2.d(detaching);
    			if (detaching) detach(t2);
    			if (if_block3) if_block3.d(detaching);
    			if (detaching) detach(t3);
    			if (if_block4) if_block4.d(detaching);
    			if (detaching) detach(t4);
    			if (if_block5) if_block5.d(detaching);
    			if (detaching) detach(if_block5_anchor);
    		}
    	};
    }

    function instance$c($$self, $$props, $$invalidate) {
    	let { page } = $$props,
    		{ returnToUrl } = $$props,
    		{ staticData } = $$props,
    		{ lightboxIsActive } = $$props,
    		{ lightboxContent } = $$props,
    		{ role } = $$props;

    	function toggleLightbox(event) {
    		$$invalidate(0, lightboxIsActive = !lightboxIsActive);

    		if (event.detail) {
    			$$invalidate(1, lightboxContent = event.detail);
    		}
    	}

    	let duoswitchData = {
    		feature: true,
    		links: [
    			{
    				label: "Inspiration",
    				url: "/inspiration",
    				active: page != "reel"
    			},
    			{
    				label: "Font ID",
    				url: "/fontid",
    				active: page == "reel"
    			}
    		]
    	};

    	$$self.$set = $$props => {
    		if ("page" in $$props) $$invalidate(2, page = $$props.page);
    		if ("returnToUrl" in $$props) $$invalidate(3, returnToUrl = $$props.returnToUrl);
    		if ("staticData" in $$props) $$invalidate(4, staticData = $$props.staticData);
    		if ("lightboxIsActive" in $$props) $$invalidate(0, lightboxIsActive = $$props.lightboxIsActive);
    		if ("lightboxContent" in $$props) $$invalidate(1, lightboxContent = $$props.lightboxContent);
    		if ("role" in $$props) $$invalidate(5, role = $$props.role);
    	};

    	return [
    		lightboxIsActive,
    		lightboxContent,
    		page,
    		returnToUrl,
    		staticData,
    		role,
    		toggleLightbox,
    		duoswitchData
    	];
    }

    class App extends SvelteComponent {
    	constructor(options) {
    		super();

    		init(this, options, instance$c, create_fragment$c, safe_not_equal, {
    			page: 2,
    			returnToUrl: 3,
    			staticData: 4,
    			lightboxIsActive: 0,
    			lightboxContent: 1,
    			role: 5
    		});
    	}
    }

    const AppComponent = new App({
      target: document.querySelector(".app"),
      props: {
        page: page,
        returnToUrl: returnToUrl,
        staticData: globalData,
        role: "user",
      },
    });

}());
