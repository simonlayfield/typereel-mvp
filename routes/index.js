const express = require("express"),
  db = require("../db"),
  cloudinary = require("cloudinary"),
  fs = require("fs"),
  upload = require("../controllers/upload"),
  average = require('image-average-color'),
  router = express.Router();

cloudinary.config({
  cloud_name: "typereel",
  api_key: "292957339393793",
  api_secret: "r8ckXAdmBhckdJxawbQeYhWLqdY",
});

// Register SSR Svelte components
require("svelte/register");

// SSR Svelte views
const submissionView = require("../views/submission.svelte").default,
  thanksView = require("../views/thanks.svelte").default,
  aboutView = require("../views/about.svelte").default,
  faqView = require("../views/faq.svelte").default,
  loginView = require("../views/login.svelte").default,
  registerView = require("../views/register.svelte").default,
  reelView = require("../views/reel.svelte").default,
  inspirationView = require("../views/inspiration.svelte").default,
  fontsView = require("../views/fonts.svelte").default,
  homeView = require("../views/home.svelte").default,
  privacyView = require("../views/privacy.svelte").default,
  error404View = require("../views/404.svelte").default;

var isAuthenticated = (req, res, next) => {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated()) return next();
  // if the user is not authenticated then redirect him to the login page
  req.session.returnTo = req.originalUrl;
  res.redirect("/login");
};

module.exports = (passport) => {
  /* GET Login page. */
  router.get("/login", (req, res) => {
    if (req.query.referrer) {
      req.session.returnTo = req.query.referrer;
    }

    // Display the Login page with any flash message, if any
    const { html, head, css } = loginView.render({
      message: req.flash("message"),
    });
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>${css.code}</style></head>
      <body>${html}
      </body>
      </html>
    `);
    res.end();
  });

  /* Handle Login POST */
  router.post(
    "/login",
    passport.authenticate("login", {
      successReturnToOrRedirect: "/",
      failureRedirect: "/login",
      failureFlash: true,
    })
  );

  /* Handle Logout */
  router.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/");
  });

  /* GET Register page */
  router.get("/register", (req, res) => {
    if (req.query.referrer) {
      req.session.returnTo = req.query.referrer;
    }

    const { html, head, css } = registerView.render({
      message: req.flash("message"),
    });
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <meta charset="utf-8">
      <head>
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>${css.code}</style>
      </head>
      <body>${html}
      </body>
      </html>
    `);
    res.end();
  });

  /* Handle Register POST */
  router.post(
    "/register",
    passport.authenticate("register", {
      successRedirect: "/",
      failureRedirect: "/register",
      failureFlash: true,
    })
  );

  router.get('/', (req, res) => {
    // Display the Login page with any flash message, if any
    const { html, head, css } = homeView.render();
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>${css.code}</style></head>
      <body>${html}
      <script>
        const page = "home",
              returnToUrl = "${req.url}",
              globalData = null;
      </script>
      <script src="/js/bundle.js"></script>
      </body>
      </html>
      
    `);
    res.end();
  });

  router.get(["/inspiration", "/inspiration", "/inspiration/page/:number"], (req, res) => {
    const page = req.params.number - 1;
    const limit = 40;
    const skip = page * limit;

    var collection = db.get().collection("inspiration");

    collection.countDocuments().then((count) => {
      const pageLength = Math.ceil(count / limit);

      collection
        .find()
        .sort({ _id: -1 })
        .skip(skip)
        .limit(limit)
        .toArray((err, docs) => {
          const { html, head, css } = inspirationView.render({
            featuredData: docs,
            username: req.user ? req.user.username : null,
            role: req.user ? req.user.role : null,
            pageNumber: req.params.number ? parseInt(req.params.number) : 1,
            lastPage: docs.length < limit ? true : false,
            pageLength: parseInt(pageLength),
          });

          res.set({ "content-type": "text/html; charset=utf-8" });
          res.write(`
          <!DOCTYPE html>
          <html>
          <head>
          <meta charset="utf-8">
          ${head}
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <style>${css.code}</style></head>
          <body>${html}
          <script>
            const page = "inspiration",
                  pageId = null,
                  returnToUrl = "${req.url}",
                  globalData = ${JSON.stringify(docs)};
          </script>
          <script src="/js/bundle.js"></script>
          </body>
          </html>
        `);
          res.end();
        });
    });
  });

  router.get(["/fontid", "/fontid/page/:number"], (req, res) => {
    const page = req.params.number - 1;
    const limit = 40;
    const skip = page * limit;

    var collection = db.get().collection("specimens");

    collection.countDocuments().then((count) => {
      const pageLength = Math.ceil(count / limit);

      collection
        .find()
        .sort({ _id: -1 })
        .skip(skip)
        .limit(limit)
        .toArray((err, docs) => {
          const { html, head, css } = reelView.render({
            data: docs,
            username: req.user ? req.user.username : null,
            pageNumber: req.params.number ? parseInt(req.params.number) : 1,
            lastPage: docs.length < limit ? true : false,
            pageLength: parseInt(pageLength),
            returnToUrl: req.url,
          });

          res.set({ "content-type": "text/html; charset=utf-8" });
          res.write(`
          <!DOCTYPE html>
          <html>
          <head>
          <meta charset="utf-8">
          ${head}
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <style>${css.code}</style></head>
          <body>${html}
          <script>
            const page = "reel",
                  pageId = null,
                  returnToUrl = "${req.url}",
                  globalData = ${JSON.stringify(docs)};
          </script>
          <script src="/js/bundle.js"></script>
          </body>
          </html>
        `);
          res.end();
        });
    });
  });

  router.get("/submission", isAuthenticated, (req, res) => {
    const { html, head } = submissionView.render({
      errorMessage: req.session["error"],
    });
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      </head>
      <body>${html}
      <script>
        const page = "submission",
              returnToUrl = "/" + page,
              globalData = null;
      </script>
      <script src="/js/bundle.js"></script>
      </body>
      </html>
    `);
    res.end();
  });

  router.get("/privacy", (req, res) => {
    const { html, head } = privacyView.render();
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      </head>
      <body>${html}
      </body>
      </html>
    `);
    res.end();
  });

  router.post("/upload", (req, res) => {
    upload.upload(req, res, (err) => {
      if (err) {
        
        req.session["error"] = err.message;
        res.redirect("/submission");
        res.end();
        return;
      }

      // Get the domaint colour of the image
      average(`./public/images/temp/${req.file.filename}`, (err, color) => {
        if (err) throw err;
        
        cloudinary.v2.uploader.upload(
          `./public/images/temp/${req.file.filename}`,
          {
            folder: "specimens",
          },
          function (error, result) {
            fs.unlink(`./public/images/temp/${req.file.filename}`, (err) => {
              if (err) throw err;
  
              const newSpecimenObj = {
                name: null,
                image: {
                  src: `${result.public_id}.${result.format}`,
                  width: result.width,
                  height: result.height,
                  average_colour: `rgba(${color[0]}, ${color[1]}, ${color[2]}, ${color[3]})`
                },
                user: req.user.username,
                credit: [
                  {
                    name: req.body.creditName ? req.body.creditName : null,
                    url: req.body.creditUrl ? req.body.creditUrl : null,
                  },
                ],
                fonts: [],
                subscribers: [req.user.username.toString()],
                comments: [],
              };
  
              if (req.body.reference) {
                const comment = {
                  body: req.body.name,
                  url: req.body.reference,
                  user: req.user.username,
                  type: "match",
                };
                newSpecimenObj.comments.push(comment);
              }
  
              db.get()
                .collection("specimens")
                .insertOne(newSpecimenObj, (err, result) => {
                  if (err) return console.log(err);
                  res.redirect("/thanks");
                });
            });
          }
        );

      });

    });
  });

  router.get("/fonts", (req, res) => {
    var collection = db.get().collection("fonts");

    collection
      .find()
      .sort({ name: 1 })
      .toArray((err, docs) => {
        const { html, head, css } = fontsView.render({
          fonts: docs,
        });
        res.set({ "content-type": "text/html; charset=utf-8" });
        res.write(`
        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8">
        ${head}
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>${css.code}</style></head>
        <body>${html}
        </body>
        </html>
      `);
        res.end();
      });
  });

  router.get("/thanks", (req, res) => {
    const { html, head, css } = thanksView.render();
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>${css.code}</style></head>
      <body>${html}
      </body>
      </html>
    `);
    res.end();
  });

  router.get("/about", (req, res) => {
    const { html, head, css } = aboutView.render();
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>${css.code}</style></head>
      <body>${html}
      </body>
      </html>
    `);
    res.end();
  });

  router.get("/faq", (req, res) => {
    const { html, head, css } = faqView.render();
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>${css.code}</style></head>
      <body>${html}
      </body>
      </html>
    `);
    res.end();
  });

  // Handle 404
  router.use(function (req, res) {
    res.status(400);
    const { html, head, css } = error404View.render();
    res.set({ "content-type": "text/html; charset=utf-8" });
    res.write(`
      <!DOCTYPE html>
      <html>
      <head>
      <meta charset="utf-8">
      ${head}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>${css.code}</style></head>
      <body>${html}
      </body>
      </html>
    `);
    res.end();
  });

  return router;
};
