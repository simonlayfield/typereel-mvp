const db = require('../db');

  function getTypereelPage(req, res) {

    var page = parseInt(req.query.num),
        size = parseInt(req.query.size),
        skip = page > 0 ? ((page - 1) * size) : 0;

    db.get().collection('specimens').find({}).skip(skip).limit(size).toArray((err, result) => {
      if (err) throw err;
      res.send(JSON.stringify({"data": result}))
    });

  };

module.exports = {
  getTypereelPage
}
