const mailjet = require("node-mailjet").connect(
  process.env.MJ_APIKEY_PUBLIC,
  process.env.MJ_APIKEY_PRIVATE
);

const sendRegisterEmail = function(user) {
  return mailjet
    .post("send", {
      version: "v3.1"
    })
    .request({
      Messages: [
        {
          From: {
            Email: "tech@typereel.io",
            Name: "Typereel"
          },
          To: [
            {
              Email: user.email
            }
          ],
          TemplateID: 748424,
          Subject: "Thanks for registering"
        }
      ]
    });
};

const sendUserNotificationEmails = (users, comment, commenter, post) => {
  const developmentMode = process.env.NODE_ENV == "development";

  let messages = [];

  for (var user of users) {
    messages.push({
      From: {
        Email: "tech@typereel.io",
        Name: "Typereel"
      },
      To: [
        {
          Email: user.email
        }
      ],
      TemplateID: 822226,
      Subject: "A user has responded on Typereel",
      TemplateLanguage: true,
      Variables: {
        test: "test value",
        sender: commenter.username,
        comment: comment,
        post: post
      }
    });
  }

  return mailjet
    .post("send", {
      version: "v3.1"
    })
    .request({
      Messages: messages,
      Sandbox: developmentMode
    });
};

const addUserAsEmailContact = email => {
  return mailjet.post("contact", { version: "v3" }).request({
    IsExcludedFromCampaigns: false,
    Email: email
  });
};

const addEmailContactToMailingList = user => {
  return mailjet.post("listrecipient", { version: "v3" }).request({
    IsUnsubscribed: true,
    ContactID: user.ID,
    ContactAlt: user.Email,
    ListID: "10159573"
  });
};

module.exports = {
  sendRegisterEmail,
  sendUserNotificationEmails,
  addUserAsEmailContact,
  addEmailContactToMailingList
};
