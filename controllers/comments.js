const express = require('express'),
      router = express.Router(),
      emails = require('../routes/emails'),
      { check, validationResult } = require('express-validator');

const db = require('../db');

const Comments = require('../models/comments'),
      Users = require('../models/users'),
      Posts = require('../models/posts');

const validate = [
    check('comment').trim().escape()
];

router.post('/addComment/:specimenType/:id', validate, (req, res) => {

  // TODO These vars are repeated which is pretty lame

  const specimenType = req.params.specimenType,
        category = specimenType === 'inspiration' ? 'feature' : 'specimen',
        post = `/${category}/${req.params.id}`;

  Comments.addComment(req, (err, result, comment) => {

    // result = successful comment object
    // comment = comment that was posted

    if (err) return console.log(err);

    // Returns an array of users to subscribe to comments on this post
    Posts.getNotificationSubscribers(specimenType, req.params.id, (notificationSubscribers) => {

      // Do not send an email to the person who is commenting
      notificationSubscribers = notificationSubscribers.filter(subscribedUser => subscribedUser !== req.user.username);

      if (notificationSubscribers && notificationSubscribers.length) {

        // Fetch email addresses of users
        Users.findUsersByUsername(notificationSubscribers, (err, users) => {

          emails.sendUserNotificationEmails(users, comment, req.user, post)
            .then((result) => {
              console.log('Email success');
            })
            .catch((err) => {
              console.log(err)
            });
          res.redirect(post);

        });
      } else {
        // No subscribers
        res.redirect(post);
      }

    });

  });

});

module.exports = () => {
  return router;
};
