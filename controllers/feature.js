const express = require("express"),
  router = express.Router();

// Register SSR Svelte components
require("svelte/register");

const db = require("../db"),
  Feature = require("../models/feature"),
  Fonts = require("../models/fonts"),
  featureView = require("../views/page.svelte").default;

// Feature pages
router.get("/:id", (req, res) => {
  Feature.findFeatureById(req, (err, featureData) => {
    // If we have identified fonts in our db
    if (featureData.identified) {
      // Create new array of font names to fetch from db
      // Must be sorted alphabetically to match returned db data
      let fontNames = featureData.identified
        .sort((a, b) => a.name.localeCompare(b.name))
        .map((font) => {
          return font.name;
        });

      Fonts.findFontsByName(fontNames, (err, fonts) => {
        // Amend identified font array to include class/style/weight
        // TODO this seems really clunky. Need to rethink how this is done.
        let identifiedCharacteristics = featureData.identified.map((font) => {
          return {
            weight: font.weight ? font.weight : null,
            style: font.style ? font.style : null,
          };
        });

        renderPage(res, req, featureData, fonts, identifiedCharacteristics);
      });
    } else {
      renderPage(res, req, featureData, null, null);
    }
  });
});

function renderPage(res, req, featureData, fonts, identifiedCharacteristics) {
  featureData.characteristics = identifiedCharacteristics;
  featureData.fonts = fonts;
  const { html, head, css } = featureView.render({
    data: featureData,
    username: req.user ? req.user.username : null,
    returnToUrl: "/feature" + req.url,
  });

  const globalUser = req.user ? '"' + req.user.username + '"' : null;

  res.set({ "content-type": "text/html; charset=utf-8" });
  res.write(`
    <!DOCTYPE html>
    <html>
    <head>${head}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>${css.code}</style></head>
    <body>${html}
    <div class="app"></div>
    <script>

      const page = "feature",
            pageId = "${req.params.id}",
            returnToUrl = "/" + page + "/" + pageId,
            globalUser = ${globalUser},
            globalData = ${JSON.stringify(featureData)};

    </script>
    <script src="/js/bundle.js"></script>
    <script src="/js/main.js"></script>
    </body>
    </html>
  `);
  res.end();
}

router.post("/toggleSubscriber", (req, res) => {
  Feature.toggleSpecimenSubscriber(req, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

module.exports = () => {
  return router;
};
