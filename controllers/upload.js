const multer = require("multer"),
      path = require('path');

const storage = multer.diskStorage({
  destination: './public/images/temp/',
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' +
      Date.now() +
      path.extname(file.originalname));
  }
});

// https://www.youtube.com/watch?v=9Qzmri1WaaE
const upload = multer({
  storage: storage,
  limits: {fileSize: 8000000},
  fileFilter: (req, file, cb) => {
    checkFileType(file, cb);
  }
}).single('specimenFile');

function checkFileType(file, cb) {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  if(extname && mimetype) {
    return cb(null, true);
  } else {
    return cb("Error: images only");
  }
};

module.exports = {
  upload
}
