const express = require("express"),
  router = express.Router();

// Register SSR Svelte components
require("svelte/register");

const db = require("../db"),
  Features = require("../models/features"),
  featuresByFontNameView = require("../views/page.svelte").default;

// Features by font
router.get("/:name", (req, res) => {
  Features.findFeaturesByFontName(req, (err, features) => {
    renderPage(res, req, features);
  });
});

function renderPage(res, req, features) {
  const { html, head, css } = featuresByFontNameView.render({
    data: features,
    username: req.user ? req.user.username : null,
    returnToUrl: "/feature" + req.url
  });

  const globalUser = req.user ? '"' + req.user.username + '"' : null;

  res.set({ "content-type": "text/html; charset=utf-8" });
  res.write(`
    <!DOCTYPE html>
    <html>
    <head>${head}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>${css.code}</style></head>
    <body>${html}
    <div class="app"></div>
    <script>

      const page = "inspiration",
            pageId = "${req.params.id}",
            returnToUrl = "/" + page + "/" + pageId,
            globalUser = ${globalUser},
            globalData = ${JSON.stringify(features)};

    </script>
    <script src="/js/bundle.js"></script>
    <script src="/js/main.js"></script>
    </body>
    </html>
  `);
  res.end();
}

module.exports = () => {
  return router;
};
