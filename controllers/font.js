const express = require("express"),
  router = express.Router(),
  Fonts = require("../models/fonts"),
  fontView = require("../views/font.svelte").default;

// Register SSR Svelte components
require("svelte/register");

router.get("/:id", (req, res) => {
  console.log(req.params.id);

  Fonts.findFontById(req.params.id, (err, font) => {
    if (err) {
      console.log(err);
      return;
    }

    renderPage(req, res, font);
  });
});

function renderPage(req, res, font) {
  const { html, head, css } = fontView.render({
    font: font,
    username: req.user ? req.user.username : null,
    returnToUrl: "/font" + req.url,
  });

  const globalUser = req.user ? '"' + req.user.username + '"' : null;

  res.set({ "content-type": "text/html; charset=utf-8" });
  res.write(`
    <!DOCTYPE html>
    <html>
    <head>${head}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>${css.code}</style></head>
    <body>${html}
    <div class="app"></div>

    <script src="/js/bundle.js"></script>
    <script src="/js/main.js"></script>
    </body>
    </html>
  `);
  res.end();
}

module.exports = () => {
  return router;
};
