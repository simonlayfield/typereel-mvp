const express = require("express"),
  router = express.Router(),
  specimenView = require("../views/specimen.svelte").default;

const Specimen = require("../models/specimens"),
  Fonts = require("../models/fonts");

router.get("/:id", (req, res) => {
  Specimen.findSpecimenById(req, (err, specimenData) => {
    if (specimenData.identified) {

      specimenData.identified = specimenData.identified.sort();

      console.log([...specimenData.identified]);
      
      console.log(specimenData.identified);

      Fonts.findFontsByName([...specimenData.identified], (err, fonts) => {
        // Amend identified font array to include class/style/weight
        // TODO this seems really clunky. Need to rethink how this is done.
        let identifiedCharacteristics = specimenData.identified.map((font) => {
          return {
            weight: font.weight ? font.weight : null,
            style: font.style ? font.style : null,
          };
        });
        
        specimenData.fonts = fonts;


        const { html, head, css } = specimenView.render({
          data: specimenData,
          returnToUrl: "/specimen" + req.url,
          username: req.user ? req.user.username : null,
          role: req.user && req.user.role ? req.user.role : null,
        });

        const globalUser = req.user ? '"' + req.user.username + '"' : null;

        res.set({ "content-type": "text/html; charset=utf-8" });
        res.write(`
          <!DOCTYPE html>
          <html>
          <head>${head}
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <style>${css.code}</style></head>
          <body>${html}
          <script>

            const page = "specimen",
                  pageId = "${req.params.id}",
                  returnToUrl = "/" + page + "/" + pageId,
                  globalUser = ${globalUser},
                  globalData = ${JSON.stringify(specimenData)};

          </script>
          <script src="/js/bundle.js"></script>
          <script src="/js/main.js"></script>
          </body>
          </html>
        `);
        res.end();
      });
    } else {
      const { html, head, css } = specimenView.render({
        data: specimenData,
        returnToUrl: "/specimen" + req.url,
        username: req.user ? req.user.username : null,
        role: req.user && req.user.role ? req.user.role : null,
      });

      res.set({ "content-type": "text/html; charset=utf-8" });
      res.write(`
        <!DOCTYPE html>
        <html>
        <head>${head}
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>${css.code}</style></head>
        <body>${html}
        <script>

          const page = "specimen",
                pageId = "${req.params.id}",
                returnToUrl = "/" + page + "/" + pageId,
                globalUser = "${req.user ? req.user.username : ""}",
                globalData = ${JSON.stringify(specimenData)};

        </script>
        <script src="/js/bundle.js"></script>
        <script src="/js/main.js"></script>
        </body>
        </html>
      `);
      res.end();
    }
  });
});

router.post("/toggleSubscriber", (req, res) => {
  Specimen.toggleSpecimenSubscriber(req, (err, result) => {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
});

// REST route for moving specimen
router.post("/move", (req, res) => {
  Specimen.moveSpecimen(req.body.specimenId).then(
    () => {
      res.send(JSON.stringify({ response: "complete" }));
    },
    () => {
      res.send(JSON.stringify({ response: "failed" }));
    }
  );
});

module.exports = () => {
  return router;
};
