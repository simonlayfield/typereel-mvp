import App from "./App.svelte";

const AppComponent = new App({
  target: document.querySelector(".app"),
  props: {
    page: page,
    returnToUrl: returnToUrl,
    staticData: globalData,
    role: "user",
  },
});

export default AppComponent;
