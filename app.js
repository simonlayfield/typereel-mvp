require("dotenv").config();

const express = require("express"),
  bodyParser = require("body-parser"),
  passport = require("passport"),
  expressSession = require("express-session"),
  flash = require("connect-flash"),
  db = require("./db"),
  PORT = process.env.PORT || 3000,
  appDb =
    process.env.NODE_ENV !== "development"
      ? `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@typereel-mvp.pokso.mongodb.net/typereel-mvp?retryWrites=true&w=majority`
      : `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@typereel-mvp.pokso.mongodb.net/typereel-mvp?retryWrites=true&w=majority`,
  LocalStrategy = require("passport-local").Strategy,
  app = express();

app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(
  expressSession({
    secret: "mySecretKey",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// Initialize Passport
var initPassport = require("./passport/init");
initPassport(passport);

app.use("/feature", require("./controllers/feature")(passport));
app.use("/features", require("./controllers/features")(passport));
app.use("/specimen", require("./controllers/specimen")(passport));
app.use("/comments", require("./controllers/comments")(passport));
app.use("/font", require("./controllers/font")(passport));
app.use("/", require("./routes/index")(passport));

db.connect(appDb, (err) => {
  if (err) {
    console.log("Unable to connect to Mongo.");
    console.log(err);
    process.exit(1);
  } else {
    app.listen(PORT, () => {
      console.log("Listening on port 3000...");
    });
  }
});
