var LocalStrategy = require("passport-local").Strategy,
  User = require("../models/user"),
  bCrypt = require("bcryptjs"),
  emails = require("../routes/emails");

module.exports = function (passport) {
  passport.use(
    "register",
    new LocalStrategy(
      {
        passReqToCallback: true,
      },
      function (req, username, password, done) {
        var findOrCreateUser = function () {
          // find a user in Mongo with provided username
          User.findOne({ username: username }, function (err, user) {
            // In case of any error return
            if (err) {
              console.log("Error in SignUp: " + err);
              return done(err);
            }
            // already exists
            if (user) {
              console.log("User already exists");
              return done(
                null,
                false,
                req.flash("message", "User Already Exists")
              );
            } else {
              // if there is no user with that email
              // create the user
              var newUser = new User();
              // set the user's local credentials
              newUser.username = username;
              newUser.password = createHash(password);
              newUser.email = req.body.email;
              newUser.code = req.body.code;

              // save the user
              newUser.save(function (err) {
                if (err) {
                  // TODO Should I have to create an array of errors?
                  // Doesn't feel quite right
                  console.dir(Object.values(err.errors));
                  // throw err;
                  return done(
                    null,
                    false,
                    req.flash("message", Object.values(err.errors))
                  );
                }

                // If requested, add to mailing list

                if (req.body.mailing_list == "Agree") {
                  emails
                    .addUserAsEmailContact(req.body.email)
                    .then((result) => {
                      emails.addEmailContactToMailingList(result.body.Data[0]);
                    })
                    .catch((err) => {
                      console.log(err);
                    });
                }

                if (process.env.NODE_ENV !== "development") {
                  emails.sendRegisterEmail(req.body).catch((err) => {
                    console.log(err.statusCode);
                  });
                }

                return done(null, newUser);
              });
            }
          });
        };

        // Delay the execution of findOrCreateUser and execute
        // the method in the next tick of the event loop
        process.nextTick(findOrCreateUser);
      }
    )
  );

  // Generates hash using bCrypt
  var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
  };
};
