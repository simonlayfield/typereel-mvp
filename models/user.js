var mongoose = require("mongoose");
var validate = require("mongoose-validator");
var bcrypt = require("bcryptjs");

var usernameValidator = [
  validate({
    validator: "isLength",
    arguments: [2, 15],
    message: "Usernames must be between {ARGS[0]} and {ARGS[1]} characters",
  }),
  validate({
    validator: "isAlphanumeric",
    message: "Usernames cannot contain special characters",
  }),
];

// User Schema
var UserSchema = new mongoose.Schema({
  username: {
    type: String,
    validate: usernameValidator,
  },
  password: {
    type: String,
  },
  email: {
    type: String,
  },
  role: {
    type: String,
    enum: ["user", "admin"],
    default: "user",
  },
});

var User = (module.exports = mongoose.model("User", UserSchema));

module.exports.createUser = function (newUser, callback) {
  bcrypt.genSalt(10, function (err, salt) {
    bcrypt.hash(newUser.password, salt, function (err, hash) {
      newUser.password = hash;
      newUser.save(callback);
    });
  });
};
