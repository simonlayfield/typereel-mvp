const db = require("../db"),
  ObjectId = require("mongodb").ObjectId;

exports.findFeatureById = (req, cb) => {
  db.get()
    .collection("inspiration")
    .findOne(ObjectId(req.params.id), (err, doc) => {
      cb(err, doc);
    });
};

module.exports.toggleSpecimenSubscriber = (req, cb) => {
  if (req.body.subscribed == "true") {
    // TODO 'specimen' is used even if this is a feature
    db.get()
      .collection("inspiration")
      .findOneAndUpdate(
        { _id: ObjectId(req.body.specimen) },
        {
          $pull: {
            subscribers: `${req.user.username}`
          }
        },
        (err, result) => {
          cb(err, result);
        }
      );
  } else {
    db.get()
      .collection("inspiration")
      .findOneAndUpdate(
        { _id: ObjectId(req.body.specimen) },
        {
          $push: {
            subscribers: `${req.user.username}`
          }
        },
        (err, result) => {
          cb(err, result);
        }
      );
  }
};
