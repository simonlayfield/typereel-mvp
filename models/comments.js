const db = require("../db"),
  ObjectId = require("mongodb").ObjectId,
  emails = require("../routes/emails");

exports.addComment = (req, cb) => {
  let comment = req.body.comment,
    specimenType = req.params.specimenType;

  function linkify(text) {
    var exp = /(https:&#x2F;&#x2F;[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|;])/gi;
    return text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
  }

  comment = comment.replace(/\n/g, "<br>\n");
  comment = linkify(comment);

  db.get()
    .collection(specimenType)
    .findOneAndUpdate(
      { _id: ObjectId(req.params.id) },
      {
        $push: {
          comments: {
            body: comment,
            user: req.user.username,
            url: req.body.url ? req.body.url : null,
            type: req.body.type ? req.body.type : "text"
          }
        }
      },
      (err, result) => {
        cb(err, result, comment);
      }
    );
};
