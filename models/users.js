const db = require('../db');

exports.findUsersByUsername = (usernames, cb) => {
  db.get().collection('users').find({username: {$in:usernames}}).toArray((err, results) => {
    cb(err, results)
  });
}
