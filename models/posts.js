const db = require('../db'),
      ObjectId = require('mongodb').ObjectId;

exports.getNotificationSubscribers = (type, id, cb) => {
  db.get().collection(type).findOne({"_id": ObjectId(id)}, (err, doc) => {
    if (err) console.log(err);
    cb(doc.subscribers);
  });
}
