var mongoose = require("mongoose");

const ObjectId = require("mongodb").ObjectId;

// Specimen Schema
var SpecimenSchema = mongoose.Schema({
  name: {
    type: String,
  },
  src: {
    type: String,
  },
  reference: {
    type: String,
  },
  name: {
    type: String,
  },
  user: {
    type: String,
  },
  comments: {
    type: Array,
  },
  subscribers: {
    type: Array,
  },
});

var Specimen = (module.exports = mongoose.model("Specimen", SpecimenSchema));

const db = require("../db");

module.exports.findSpecimenById = (req, cb) => {
  db.get()
    .collection("specimens")
    .findOne(ObjectId(req.params.id), (err, doc) => {
      cb(err, doc);
    });
};

module.exports.toggleSpecimenSubscriber = (req, cb) => {
  if (req.body.subscribed == "true") {
    db.get()
      .collection("specimens")
      .findOneAndUpdate(
        { _id: ObjectId(req.body.specimen) },
        {
          $pull: {
            subscribers: `${req.user.username}`,
          },
        },
        (err, result) => {
          cb(err, result);
        }
      );
  } else {
    db.get()
      .collection("specimens")
      .findOneAndUpdate(
        { _id: ObjectId(req.body.specimen) },
        {
          $push: {
            subscribers: `${req.user.username}`,
          },
        },
        (err, result) => {
          cb(err, result);
        }
      );
  }
};

module.exports.moveSpecimen = async (specimenId, cb) => {
  // Move specimen (document) from specimens collection
  // to inspiration

  const sourceDocs = await db
    .get()
    .collection("specimens")
    .find(ObjectId(specimenId));

  console.log(`Moving...`);

  const idsOfCopiedDocs = await insertDocuments(
    db.get().collection("inspiration"),
    sourceDocs
  );

  const targetDocs = await db
    .get()
    .collection("inspiration")
    .find({
      _id: { $in: idsOfCopiedDocs },
    });
  // await deleteDocuments(db.get().collection("specimens"), targetDocs);

  return Promise.resolve(1);

  async function insertDocuments(collection, documents) {
    const insertedIds = [];
    const bulkWrites = [];

    await documents.forEach((doc) => {
      const { _id } = doc;

      insertedIds.push(_id);
      bulkWrites.push({
        replaceOne: {
          filter: { _id },
          replacement: doc,
          upsert: true,
        },
      });
    });

    if (bulkWrites.length)
      await collection.bulkWrite(bulkWrites, { ordered: false });

    return insertedIds;
  }

  // We're not deleting documents for now, but we may do in future
  async function deleteDocuments(collection, documents) {
    const bulkWrites = [];

    await documents.forEach(({ _id }) => {
      bulkWrites.push({
        deleteOne: {
          filter: { _id },
        },
      });
    });

    if (bulkWrites.length)
      await collection.bulkWrite(bulkWrites, { ordered: false });
  }
};
