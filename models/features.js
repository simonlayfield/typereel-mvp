const db = require("../db");

exports.findFeaturesByFontName = (req, cb) => {
  const UrlName = req.params.name.replace(/-/g, " ");

  db.get()
    .collection("inspiration")
    .find({ identified: { $elemMatch: { name: UrlName } } })
    .toArray((err, results) => {
      cb(err, results);
    });
};
