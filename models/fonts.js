const db = require("../db"),
  ObjectId = require("mongodb").ObjectId;

exports.findFontsByName = (fontNames, cb) => {
  // Fetch font(s) by name
  db.get()
    .collection("fonts")
    .find({ name: { $in: fontNames } })
    .sort({ name: 1 })
    .toArray((err, fonts) => {
      cb(err, fonts);
    });
};

exports.findFontById = (fontId, cb) => {
  // Fetch font(s) by name
  db.get()
    .collection("fonts")
    .findOne({ _id: ObjectId(fontId) }, (err, font) => {
      cb(err, font);
    });
};
