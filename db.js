var mongoose = require("mongoose");

var state = {
  db: null
};

mongoose.set("useCreateIndex", true);

exports.connect = (url, done) => {
  if (state.db) return done();

  mongoose.connect(
    url,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err, db) => {
      if (err) return done(err);
      state.db = mongoose.connection;
      done();
    }
  );
};

exports.get = () => {
  return state.db;
};

exports.close = done => {
  if (state.db) {
    state.db.close((err, result) => {
      state.db = null;
      state.mode = null;
      done(err);
    });
  }
};
