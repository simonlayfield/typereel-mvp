const gulp = require('gulp');
const postcss = require('gulp-postcss');
const postcssNesting = require('postcss-nesting');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');

gulp.task('css', (done) => {
  gulp.src('src/css/*')
    .pipe(concat('main.css'))
    .pipe(postcss([postcssNesting()]))
    .pipe(cleanCSS({compatibility: 'ie11'}))
    .pipe(gulp.dest('public/css'));
  done();
});

gulp.task('watch', () => {
  gulp.watch(['src/css/*'], gulp.series('css'));
});

gulp.task("default", gulp.series("watch"));